#
# ~/.config/mutt/muttrc
#

# User variables
# ----
set my_mutt_home = "~/.config/mutt"
# ----

# Misc
# ----
# Remove delay in switching mailboxes.
set sleep_time = 0

# Don't ask for recipient and subject confirmation for mail replies.
set fast_reply = yes
set include = yes

# Show spam score (from SpamAssassin only) when reading a message.
spam "X-Spam-Score: ([0-9\\.]+).*" "SA: %1"
set pager_format = " %C ─ [%[%F %H:%M]] %.20v, %s%* %?H? [%H] ?"

# Check for all subscribed IMAP folders.
set imap_check_subscribed

# Change the temporary files directory.
# set tmpdir = "~/.cache/mutt"

set attach_save_dir = "~/attachments"
# ----

# Mime settings
# ----
# Enable rendering HTML & displaying as plaintext.
auto_view text/html

# Set preferred rendering for multipart messages.
# alternative_order text/plain text/enriched text/html
# ----

# Necessary sources
# ----
# Source miscellaneous variables, escaping variables meant for the shell.
source "gpg -dq $my_mutt_home/muttvars.gpg |"

# Enable GPG encryption.
source "/usr/share/doc/mutt/samples/gpg.rc"

# Enable handling of compressed & encrypted mailboxes.
# source "/usr/share/doc/mutt/samples/sample.muttrc-compress"

# Enable smime support.
# source "/usr/share/doc/mutt/samples/smime.rc"

# Enable colour support.
source "/usr/share/doc/mutt/samples/colors.linux"

# Source user-defined macros.
source "$my_mutt_home/macros"
# ----

# Sidebar
# ----
# set sidebar_visible = yes
set sidebar_width = 20

# Abbreviate mailbox paths.
set sidebar_short_path = yes

# Path separator for abbreviated mailbox paths.
set sidebar_delim_chars = '/.'

# Indent abreviated path.
set sidebar_folder_indent = yes

# Mailbox paths indentation string.
set sidebar_indent_string = '  '

# Display mailboxes with new, or flagged mail.
set sidebar_new_mail_only = no

# Always be visible, even if sidebar_new_mail_only is enabled.
sidebar_whitelist "$my_mail_dir/$my_mailbox1"
sidebar_whitelist "$my_mail_dir/$my_mailbox2"

# Enable extended buffy mode to calculate total, new, and flagged message
# counts for each mailbox.
set mail_check_stats
# ----

# Keybindings
# ----
# Move the highlight to the previous mailbox.
bind index,pager \Cp sidebar-prev

# Move the highlight to the next mailbox.
bind index,pager \Cn sidebar-next

# Open the highlighted mailbox.
bind index,pager \Co sidebar-open

# Move the highlight to the previous page.
# Useful if you have a LOT of mailboxes.
bind index,pager <F4> sidebar-page-up

# Move the highlight to the next page.
# Useful if you have a LOT of mailboxes.
bind index,pager <F5> sidebar-page-down

# Move the highlight to the previous mailbox containing new, or flagged, mail.
bind index,pager <F6> sidebar-prev-new

# Move the highlight to the next mailbox containing new, or flagged, mail.
bind index,pager <F7> sidebar-next-new

# Toggle the visibility of the Sidebar.
bind index,pager B sidebar-toggle-visible
# ----

# IMAP setup
# ----
# Check for all subscribed IMAP folders.
set imap_check_subscribed

# Store message headers locally to speed things up.
# If hcache is a folder, Mutt will create sub cache folders for each account
# which may speeds things up even more.
set header_cache = "\$HOME/.cache/mutt"

# Store messages locally to speed things up, like searching message bodies.
# Can be the same folder as header_cache.
# This will cost important disk usage according to your e-mail amount.
set message_cachedir = "\$HOME/.cache/mutt"

# Allow Mutt to open a new IMAP connection automatically.
unset imap_passive

# For locally synced mailoxes.
set mbox_type = Maildir

# Keep the IMAP connection alive by polling intermittently (time in seconds).
set imap_keepalive = 300

# How often to check for new mail (time in seconds).
set mail_check = 120
# ----

# Mailbox config
# ----
# Mailbox1.
source "$my_mutt_home/mailbox1"
folder-hook $folder "source $my_mutt_home/mailbox1"

# Mailbox2.
source "$my_mutt_home/mailbox2"
folder-hook $folder "source $my_mutt_home/mailbox2"

set from = "$my_mailbox1_user"
alternates "$my_mailbox1_user|$my_mailbox2_user"
# ----

# SMTP setup
# ----
# Character set.
set send_charset = "utf-8"
# Assume windows character set when none is defined.
set assumed_charset = "iso-8859-1"
# ----

# PGP settings
# ----
# set crypt_autosmime = no
set crypt_autosign = yes
set crypt_opportunistic_encrypt = yes
set crypt_replyencrypt = yes
set pgp_auto_decode = yes
set pgp_default_key = "$my_pgp_identity"
# ----

# Index settings
# ----
set sort = threads
set sort_aux = last-date-received
set date_format = "%F"
set index_format = "%4C [%Z%?H?|%H?] %d  %-18.18L (%?l?%4l&%4c?)  %s"
# ----

# Status settings
# ----
set status_format = "[%f Mode:%r]───[Msgs:%?M?%M/?%m%?n? New:%n?%?o? Old:%o?%?d? Del:%d?%?F? Flag:%F?%?t? Tag:%t?%?p? Postponed:%p?%?B? Background:%B?%?l? Size:%l?]%>─(%s/%S)───(%P)"
# ----

# Header customization
# ----
# Enable customizable email headers.
set edit_headers = yes

# Extra info.
my_hdr X-Info: Keep It Simple, Stupid.

# OS Info.
my_hdr X-Operating-System: Blah blah blah.

# This header only appears to MS Outlook users
my_hdr X-Message-Flag: WARNING!! Outlook sucks

# Custom Mail-User-Agent ID.
my_hdr User-Agent: Every email client sucks, but mine sucks less.
# ----

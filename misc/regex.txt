/(RegEx)/
	
Match at beginning of line: ^\s :Find space at start of line
Match at end of line: \s$ :Find space at end of line

digit [0...9]: \d
digit [0...9]: \d
Not digit: \D
word: \w
Not word: \W
Word boundary: \b
Not word boundary: \B
whitespace: \s
Not whitespace: \S
Tab space: \t
Carriage return: \r
Line feed: \n
Not line break: \N
Horizontal whitespace: \h
Not Horizontal whitespace: \H
Vertical tab: \v
Not Vertical tab: \V
Tagged region: \1 to \9 :\1 is used to backrefere
Escaped characters(\sth): . \ + * ? ^ $ [ ] { } ( ) | /

Match one or more occurrences: \sth+	:sa+ne -> sane, saane, sa...ne
Match Zero or more occurrences: sth*	:sa*ne -> sne, sane, sa...ne
Match zero or one occurence: \sth? :sa?ne -> sne, sane :Makes preceding quantifier lazy, causing it to match as few characters as possible
Match any character except line breaks: . :all except "/(\r)/"

Group multiple tokens: () :Creates a capture group for extracting a substring or using backreference
Non-capturing group: (?:) :Groups tokens without creating a capture group
Positive lookahead: (?=) :Specifies a group that can match after the main group, but doesn't include it in the result
Negative lookahead: (?!) :Specifies a group that cannot match after the main group, whose result is discarded if it matches
Positive lookbehind: (?<=) :Specifies a group that can match before the main group without including it in the result
Negative lookbehind: (?<!) :Specifies a group that cannot match before the main expression, whose result is discarded if it matches

***Combos***
Match any: [\s\S]
Find duplicates: ^(.*?)$\s+?^(?=.*^\1$)
	# Match a line beginning with any character except a line break enclosed in a capture group
	# Separated by whitesace(s) (newline, space, tab)
	# Matched with a capture group after the main group without including it(matched group) in the result (positive lookahead)
	# With any character not a line break preceeding it
	# Matching the result of the capture group main (backreferenced)
Match set: [ABC]
Match not in set: [^ABC]
Range: [A-Z] (Uppercase), [A-z] (Upper & lowercase), [a-z] (lowercase)

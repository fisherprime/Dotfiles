#!/usr/bin/env bash

# Build a kernel's initramfs & group the build output.

args=('--force' '--no-hostonly-cmdline')

while read -r line; do
	if [[ "$line" == 'usr/lib/modules/'+([^/])'/pkgbase' ]]; then
		read -r pkgbase <"/${line}"
		kver="${line#'usr/lib/modules/'}"
		kver="${kver%'/pkgbase'}"

		install -Dm0644 "/${line%'/pkgbase'}/vmlinuz" "/boot/vmlinuz-${pkgbase}"

		{
			output="\nImage: ${pkgbase}\n"
			while read -r dline; do
				output+="$dline\n"
			done < <(dracut "${args[@]}" --hostonly "/boot/initramfs-${pkgbase}.img" --kver "$kver")

			printf "%b" "$output"
		} &

		{
			output="\nImage: ${pkgbase}-fallback\n"
			while read -r dline; do
				output+="$dline\n"
			done < <(dracut "${args[@]}" --no-hostonly "/boot/initramfs-${pkgbase}-fallback.img" --kver "$kver")

			printf "%b" "$output"
		} &

		wait
	fi
done

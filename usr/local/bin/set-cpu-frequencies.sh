#!/bin/bash

# Alter the CPU maximum frequency to a user specified value.
#
# Arguments: <frequency>
#
# This operation expects a single argument, else it fails.
#
# Any value exceeding the cpuinfo provided limits will cause the frequency to
# revert to the defaults.
set_max_freq() {
	scaling_min_freq="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq)"
	[[ "$2" -ge "$scaling_min_freq" ]] &&
		tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_max_freq <<<"$2"
}

# Alter the CPU minimum frequency to a user specified value.
#
# Arguments: <frequency>
#
# This operation expects a single argument, else it fails.
#
# Any value exceeding the cpuinfo provided limits will cause the frequency to
# revert to the defaults.
set_min_freq() {
	scaling_max_freq="$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq)"
	[[ "$2" -le "$scaling_max_freq" ]] &&
		tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_min_freq <<<"$2"
}

[[ $# -lt 2 ]] &&
	printf "Usage: set-cpu-frequencies.sh -<max/min> <frequency>" &&
	return 1

case "$1" in
-max)
	set_max_freq "$@"
	;;
-min)
	set_min_freq "$@"
	;;
*)
	printf "set-cpu-frequencies.sh: Unknown operation"
	;;
esac

set history save on
set print pretty on

set disassembly-flavor intel

# Enable enhanced golang support.
add-auto-load-safe-path /usr/lib/go/src/runtime/runtime-gdb.py

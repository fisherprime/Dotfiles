#!/bin/bash

# Depends on: imagemagick, util-linux, swaylock, grim, bc
# REF: https://www.imagemagick.org/Usage/blur/

screenshot="$HOME/.cache/ws-background.png"
blurred_screenshot="$HOME/.cache/ws-background-blur.png"
blur_level="7"

_cleanup() {
	[[ -f $screenshot ]] && rm "$screenshot"
	[[ -f $blurred_screenshot ]] && rm "$blurred_screenshot"
}

_usage() {

	printf "Usage: swaylock-color [options]

Options:
    -h, --help   Print this help message
    -b, --blur   Set the blur level [1-10]
                 Default: 5
"
}

_blur_image() {
	# NOTE: This is a typical (slow) implementation.
	# convert "$screenshot" -channel RGBA -blur "$blur_level" \
	# "$blurred_screenshot"

	# NOTE: This is a faster implementation for visually percieved blurriness
	# levels.
	# The screenshot is shrunk to half its original size, blurred, then scaled
	# up to its original size.
	#
	# Using the same blur level as in the slow implementation yields a "2x"
	# blur on the image compared to the slow implementation.
	convert "$screenshot" -filter Gaussian -resize 50% \
		-define filter:sigma="$blur_level" -resize 200% "$blurred_screenshot"
}

generate() {
	if [[ $# -gt 0 && $(pgrep -c swaylock) -lt 1 ]]; then
		# shellcheck disable=SC2140
		options=$(getopt -o h,b -l "help","blur" -n "swaylock-color" -- "$@")

		# shellcheck disable=SC2181
		if [[ $? -ne 0 ]]; then
			printf "Failed parsing command options\n"
			_usage

			return 1
		fi

		eval set -- "$options"

		while true; do
			case $1 in
			-h | --help)
				_usage
				return 0
				;;
			-b | --blur)
				shift
				# Shift over `--`
				shift

				blur_level=$(bc <<<"scale=4; $1/2")

				break
				;;
			*)
				_usage
				return 1
				;;
			esac
			shift
		done
	fi

	_cleanup

	# Screenshot the desktop
	grim "$screenshot"

	_blur_image

	# Run swaylock
	swaylock -i "$blurred_screenshot" -f

	_cleanup
}

generate "$@"

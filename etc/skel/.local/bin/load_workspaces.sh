#!/bin/bash

# Save
# i3-save-tree --workspace <N> > ~/.i3_layouts/ws-<N>.json

if [[ -d "$HOME/.i3_layouts" ]]; then
	for file in "$HOME"/.i3_layouts/*.json; do
		workspace_id="$(echo "$file" | sed -E "s/.*ws-([0-9]*)\.json/\1/")"

		# Debug
		# echo i3-msg "\"workspace $workspace_id; append_layout $file\""

		i3-msg "workspace $workspace_id; append_layout $file"

		case $workspace_id in
		1)
			(claws-mail &)
			(syncthing-gtk &)
			(telegram-desktop &)
			(urxvt &)
			;;
		2)
			(waterfox &)
			;;
		3)
			(dev-docs &)
			;;
		4)
			(firefox-developer-edition &)
			(nautilus &)
			;;
		esac
	done
fi

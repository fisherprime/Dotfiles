#!/bin/bash

# Works with Xorg only.

# TODO: verify necessity
# while read -r val; do
# USER_LIST+=("$val")
# done<<<"$(users)"
#
# run_in_x() {
# [[ $# -eq 0 ]] && return
#
# [[ "${#USER_LIST}" -eq 0 ]] && return
#
# for user in "${USER_LIST[@]}"; do
# local user_x_sessions
# while IFS=$'\n' read -r val; do
# user_x_sessions+=("$val")
# done<<<"$(pgrep -U "$user" -a xinit)"
#
# [[ "${#user_x_sessions}" -eq 0 ]] && return
#
# for x_session in "${user_x_sessions[@]}"; do
# local display
# # shellcheck disable=SC2001
# display=$(sed 's/.* \(:[0-9]\) .*/\1/g' <<<"$x_session")
#
# echo $user $display "$*"
# # FIXME, something isn't ok here
# su "$user" -c "DISPLAY=$display \
# XAUTHORITY=\"/home/$user/.Xauthority\" \
# $*"

#
# # Test
# # su "$user" -c "DISPLAY=$display \
# # XAUTHORITY=\"/home/$user/.Xauthority\" \
# # env | sort"

# done
# done
# }

enable_nvidia_dgpu() {
	# shellcheck disable=SC1004
	sudo sh -c 'setpci -s 01:00.0 0x488.l=0x2000000:0x2000000; \
		echo 1 | tee /sys/bus/pci/devices/0000:00:01.0/rescan >/dev/null; \
		modprobe nvidia NVreg_UsePageAttributeTable=1; \
		modprobe nvidia-drm modeset=1'
}

disable_nvidia_dgpu() {
	# Requires bbswitch

	# shellcheck disable=SC1004
	sudo sh -c 'rmmod nvidia-drm; \
		rmmod nvidia-modeset; \
		rmmod nvidia; \
		tee /proc/acpi/bbswitch <<<OFF'
}

connect_output() {
	# Debug
	# intel-virtual-output -Sbv &

	(intel-virtual-output -b &)

	enable_nvidia_dgpu

	# Delay to allow intel-virtual-output set up
	sleep 5

	xrandr --output VIRTUAL1 --auto
}

disconnect_output() {
	if [[ $(pgrep -c intel-virtual-o) -gt 0 ]]; then
		pgrep intel-virtual-o | xargs kill -9
	fi

	# For `-f` flag only
	[[ $2 == "-f" ]] && disable_nvidia_dgpu
}

on() {
	# Enable HDMI and Display port outputs
	log_i 'Enabling dGPU outputs'

	# Cleanup on Interrupt and exit
	trap "off" SIGINT SIGQUIT SIGABRT

	connect_output

	return "$SUCCESS_RET"
}

off() {
	log_i 'Disabling outputs wired to dGPU'

	disconnect_output "$@"

	# Unset trap
	trap - SIGINT SIGTERM SIGQUIT SIGABRT EXIT

	return "$SUCCESS_RET"
}

if [[ $# -eq 1 ]]; then
	$1
else
	$1 "$@"
fi

#
# ~/.local/functions/coding.sh
#

code_mode() {
	# Sets the environment up for coding and compiling tasks
	# Specify GCC, G++, Python, Java versions
	# Enable|Disable ccache

	local command_name
	command_name="$(basename "$0")"

	local command_flags
	local usrOpt

	#local itr
	local temp
	local cnt

	local avail_gcc_versions
	local default_gcc_version
	#local COUNT_GCC

	# Refers to G++
	local avail_gpp_versions
	local default_gpp_version
	#local COUNT_GPP

	local avail_py_versions
	local default_py_version
	#local COUNT_PY

	local avail_java_versions
	#local default_java_version

	# Default binaries set to the most recent they have been the defaults for
	# my case

	_usage() {

		printf "[+] Usage: %s [options]

Options:
	-h,--help:    Print this help message
	--cache:      ccache
	--ncache:     Disable ccache
	--cc:         gcc
	--cpp:        g++
	--py:         python
	--java:       java VM
" "$command_name"

	} # End _usage

	if [[ $# -lt 1 ]]; then
		_usage
		log_i "$command_name exit with status: Invalid parameters"

		return 1
	fi

	local options
	# shellcheck disable=SC2140
	options=$(getopt -o h -l "help","cache","ncache","cc","cpp","py","java" -n "$command_name" -- "$@")

	# shellcheck disable=SC2181
	if [[ $? -ne 0 ]]; then
		printf '[!] Failed parsing command options\n'
		_usage
		return 1
	fi

	eval set -- "$options" # Don't know what this does

	while true; do
		case $1 in
		-h | --help)
			_usage
			log_i "$command_name exit with status: Success"

			return 0
			;;
		--cache)
			if [[ $command_flags == *".--ncache"* ]]; then
				printf '[!] Ambiguous ccache options\n'
				_usage
				log_w "$command_name exit with status: Invalid parameters"

				return 1
			fi

			command_flags+=".$1"
			;;
		--ncache)
			if [[ $command_flags == *".--cache"* ]]; then
				printf '[!] Ambiguous ccache options\n'
				_usage
				log_w "$command_name exit with status: Invalid parameters"

				return 1
			fi

			command_flags+=".$1"
			;;
		--cc)
			avail_gcc_versions=""

			for chk in /bin/gcc-[0-9]*; do # Also handle gcc-10+
				[[ -x $chk ]] || continue

				temp="$(basename "$chk" | sed -E 's/gcc-//g')"
				avail_gcc_versions+="[$temp],"
			done

			default_gcc_version="$(/bin/gcc --version | grep -E '^gcc' | sed -E 's/\..*//; s/.*\) //')"
			avail_gcc_versions+="[$default_gcc_version]"

			command_flags+=".$1"
			;;
		--cpp)
			avail_gpp_versions=""

			for chk in /bin/g++-[0-9]*; do
				[[ -x $chk ]] || continue

				temp="$(basename "$chk" | sed -E 's/g\+\+-//g')"
				avail_gpp_versions+="[$temp],"
			done

			default_gpp_version="$(/bin/g++ --version | grep -E '^g\+\+' | sed -E 's/\..*//; s/.*\) //')"
			avail_gpp_versions+="[$default_gpp_version]"

			command_flags+=".$1"
			;;
		--py)
			cnt=0
			avail_py_versions=""

			for chk in /bin/python[0-9]; do
				[[ -x $chk ]] || continue

				temp="$(basename "$chk" | sed -E 's/python//g')"

				[[ $cnt -gt 0 ]] && avail_py_versions+=","

				avail_py_versions+="[$temp]"

				cnt="$((cnt + 1))"
			done

			default_py_version=$temp

			command_flags+=".$1"
			;;
		--java)
			cnt=0
			avail_java_versions=""

			for chk in /usr/lib/jvm/java-[0-9]*; do
				[[ -d "$chk" ]] || continue

				temp="$(basename "$chk" | sed -E 's/java\-//; s/\-.*//')"

				[[ $cnt -gt 0 ]] && avail_java_versions+=","

				avail_java_versions+="[$temp]"

				cnt="$((cnt + 1))"
			done

			#default_java_version="$(/bin/java --version | grep -E '^\S+ [0-9.]+\s' | sed -E 's/^\S+ //; s/\..*//')"
			command_flags+=".$1"
			;;
		--)
			break
			;;
		*)
			_usage
			log_w "$command_name exit with status: Invalid parameters"

			return 1
			;;
		esac
		shift # Shift positional parameters
	done

	printf '[*] Setting up code environment\n'

	while [[ $command_flags != "" ]]; do
		case $command_flags in
		*.--cache*)
			if [[ -x /usr/lib/ccache/bin/g++ ]]; then
				printf '[+] ccache usage enabled\n'

				export PATH=$CODE_PATH
			else
				printf "[+] ccache doesn't exist on this system\n"
			fi
			command_flags="$(echo "$command_flags" | sed -E 's/\.--cache//')"
			;;
		*.--ncache*)
			if [[ -x /usr/lib/ccache/bin/g++ ]]; then
				printf '[+] ccache usage disabled\n'

				export PATH=$DEF_PATH
			else
				printf "[+] ccache doesn't exist on this system\n"
			fi
			command_flags="$(echo "$command_flags" | sed -E 's/\.--ncache//')"
			;;
		*.--cc*)
			printf '[?] What GCC version would you like to use\n'
			printf '%s: ' "$avail_gcc_versions"
			read -r usrOpt

			case $avail_gcc_versions in
			*[$usrOpt]*)
				export CC="/bin/gcc-$usrOpt"

				if [[ $usrOpt -eq $default_gcc_version ]]; then
					[[ "$(type gcc)" == *"alias"* ]] && unalias gcc
				else
					# shellcheck disable=SC2139
					alias gcc="/bin/gcc-$usrOpt"
				fi
				;;
			*)
				printf '[!] Invalid GCC choice\n'
				;;
			esac

			command_flags="$(echo "$command_flags" | sed -E 's/\.--cc//')"
			;;
		*.--cpp)
			printf '[?] What G++ version would you like to use\n'
			printf '%s: ' "$avail_gpp_versions"
			read -r usrOpt

			case $avail_gpp_versions in
			*[$usrOpt]*)
				export CXX="/bin/g++-$usrOpt"

				if [[ $usrOpt -eq $default_gpp_version ]]; then
					[[ "$(type g++)" == *"alias"* ]] && unalias g++
				else
					# shellcheck disable=SC2139
					alias g++="/bin/g++-$usrOpt"
				fi
				;;
			*)
				printf '[!] Invalid G++ choice\n'
				;;
			esac

			command_flags="$(echo "$command_flags" | sed -E 's/\.--cpp//')"
			;;
		*.--py*)
			printf '[?] What Python version would you like to use\n'
			printf '%s: ' "$avail_py_versions"
			read -r usrOpt

			case $avail_py_versions in
			*[$usrOpt]*)
				if [[ $usrOpt -eq $default_py_version ]]; then
					[[ "$(type python)" == *"alias"* ]] && unalias python
				else
					# shellcheck disable=SC2139
					alias python="/bin/python$usrOpt"
				fi
				;;
			*)
				printf '[!] Invalid Python choice\n'
				;;
			esac

			command_flags="$(echo "$command_flags" | sed -E 's/\.--py//')"
			;;
		*.--java*)
			printf '[?] What Java VM version would you like to use\n'
			printf '%s: ' "$avail_java_versions"
			read -r usrOpt

			case $avail_java_versions in
			*[$usrOpt]*)
				prev_java_home="$JAVA_HOME"

				# NOTE: Use Archlinux' java environment switcher.
				if [[ -x $(command -v archlinux-java) ]]; then
					sudo archlinux-java set "java-$usrOpt-openjdk"
					break
				fi

				export JAVA_HOME="/usr/lib/jvm/java-$usrOpt-openjdk"
				CODE_PATH=${CODE_PATH/$prev_java_home/$JAVA_HOME}
				DEF_PATH=${DEF_PATH/$prev_java_home/$JAVA_HOME}
				PATH=${PATH/$prev_java_home/$JAVA_HOME}
				;;

			*)
				printf '[!] Invalid Java VM choice\n'
				;;
			esac

			command_flags="$(echo "$command_flags" | sed -E 's/\.--java//')"
			;;
		esac
	done

	printf '[+] Code environment set\n'

	return 0
} # End code_mode

docker_control() {
	_list_dependent_images() {
		# Argument is the image ID
		# shellcheck disable=SC2046
		docker inspect --format='{{.Id}} {{.Parent}}' $(docker images --filter since="$1" -q)
	} # _list_dependent_images

	_cleanup() {
		# shellcheck disable=SC2046
		docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
	} # _cleanup
}  # End docker_control

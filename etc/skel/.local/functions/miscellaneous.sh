#
# ~/.local/functions/miscellaneous.sh
#

# NOTE: `ARRAY_START` should be defined in "/etc/shell-functions".

unalias_stuff() {
	unalias_list=("sl")

	for cmd in "${unalias_list[@]}"; do
		[[ $(command -v "$cmd") == "alias"* ]] && unalias "$cmd"
	done
} # End unalias_stuff

# etherpad_tasks() {
# printf '[*] Running etherpad tasks\n'
#
# # List all mysql pads
# mysql -u USER -pPSW etherpad_lite -e 'select store.key from store' |
# grep -Eo '^pad:[^:]+' |
# sed -e 's/pad://' |
# sort |
# uniq -c |
# sort -rn |
# awk '{if ($1!="2") {print $2 }}'
#
# # List all sqlite3 pads
# sqlite3 ./pad.db 'select store.key from store' |
# grep -Eo '^pad:[^:]+' |
# sed -e 's/pad://' |
# sort |
# uniq -c |
# sort -rn |
# awk '{if ($1!="2") {print $2 }}'
#
# # List al PostgreSQL pads
# # create or replace view v_pads as select substring(key from 'pad:(.*):revs:0') as name from store where key ~ 'pad:.*:revs:0' order by key; select * from v_pads;
# } # End etherpad_tasks

man() {
	# Colourize man output

	env \
		LESS_TERMCAP_mb="$(printf '\e[1;31m')" \
		LESS_TERMCAP_md="$(printf '\e[1;31m')" \
		LESS_TERMCAP_me="$(printf '\e[0m')" \
		LESS_TERMCAP_se="$(printf '\e[0m')" \
		LESS_TERMCAP_so="$(printf '\e[1;44;33m')" \
		LESS_TERMCAP_ue="$(printf '\e[0m')" \
		LESS_TERMCAP_us="$(printf '\e[1;32m')" \
		man "$@"
} # End man

fortunecow() {
	# Pass fortune messages to "random" cows

	# NOTE: Set this option to a positive integer >0: 72, 74, 80 if word wrapping is desired.
	local wrap_column=0

	# Using local to avoid the global exposure of variables
	local cowfile=$RANDOM
	local cow_state=$RANDOM

	local cow_list
	local ignore_list

	ignore_list=(
		"beavis.zen"
		"bong"
		"head-in"
		"hellokitty"
		"kiss"
		"satanic"
		"sodomized"
		"telebears"
		"three-eyes"
	)

	_get_cow_list() {
		local list

		IFS=' ' read_array_cmd list <<<"$(cowsay -l | grep -v : | tr "\n" " ")"

		# Debug
		# printf "Original list: %s\n" "${list[*]}"

		for ignore in "${ignore_list[@]}"; do
			list=("${list[@]/$ignore/}")
		done

		# Remove empty entries
		for item in "${list[@]}"; do
			# Debug
			# printf "len: %s, list: %s\n" "${#cow_list[*]}" "${cow_list[*]}"

			# HACK: Don;t append to an empty array; rather, set its first
			# item.
			if [[ -n $item ]]; then
				[[ "${#cow_list[*]}" -eq 0 ]] && cow_list=("$item") &&
					continue
				cow_list=("${cow_list[@]}" "$item")
			fi
		done

		# Debug
		# printf "Trimmed list: %s\n" "${cow_list[*]}"
	}

	_get_cow_list

	# Exit on empty cow list.
	[[ ${#cow_list[@]} -lt 1 ]] && return

	state_list=(
		"-b" # Cyborg
		"-d" # Dead
		"-g" # Greedy
		"-p" # Paranoia
		"-s" # Stoned
		"-t" # Tired
		"-w" # Wired (opposite of tired)
		"-y" # Youthful
	)

	# NOTE: Should a division by zero error occur, no cow was found.
	cowfile="${cow_list[$(((cowfile % ${#cow_list[*]}) + ARRAY_START))]}"
	cow_state="${state_list[$(((cow_state % ${#state_list[*]}) + ARRAY_START))]}"

	# Debug
	# printf "ARRAY_START: %s, cow_list_len: %s, cowfile: %s, state_list: %s, cow_state: %s" \
	# "$ARRAY_START" "${#cow_list[*]}" "$cowfile" "${state_list[*]}" "${cow_state}"

	# Choose a fortune from all maxims --including offensive ones--, then pipe them to cowsay,
	# wordwrapped for 78 effective columns including Cowsay formatting
	[[ $wrap_column -gt 0 ]] &&
		fortune -a | cowsay -w "$wrap_column" "$cow_state" -f "$cowfile" &&
		return 0

	fortune -a | cowsay -n "$cow_state" -f "$cowfile"

	return 0
} # End fortunecow

#
# ~/.local/functions/hardware-stats.sh
#

blue_audio() {
	# Enable Bluetooth iface & start Bluetooth control

	printf '[*] Bluetooth audio playback\n'
	sudo sh -c 'rfkill unblock bluetooth; bluetoothctl; rfkill block bluetooth'
} # End blue_audio

bluetooth_test() {
	# Check Bluetooth state.

	if [[ "$(rfkill -o SOFT list bluetooth)" == *"unblocked"* ]]; then
		power_state="$(bluetoothctl show | grep Powered | cut -d : -f 2)"

		[[ $power_state == "" ]] && printf '\n' && return

		if [[ $power_state =~ \s?yes ]]; then
			printf '[I] Bluetooth is on\n'
			return
		fi
		printf '[I] Bluetooth is off\n'

		return
	fi

	printf '[I] Bluetooth is blocked\n'
} # End bluetooth_test

nvidia_temp() {
	# Print GPU temperature

	local gpu_state=0

	if [[ -e /proc/acpi/bbswitch ]]; then
		# Bumblebee is installed.
		[[ "$(cut -f 2 -d " " /proc/acpi/bbswitch)" == "ON" ]] && gpu_state=1
	else
		# No bumblebee.
		[[ $(($(pgrep -c nvidia) + 0)) -gt 1 ]] && gpu_state=1
	fi

	if [[ $gpu_state == "1" ]]; then
		IFS="," read_array_cmd metrics <<<"$(nvidia-smi --query-gpu=name,index,temperature.gpu,power.draw --format=csv,noheader,nounits)"

		# shellcheck disable=SC2154 # Set in the line above
		[[ ${metrics[$ARRAY_START + 0]} != "NVIDIA "* ]] &&
			printf 'GPU unavailable (failure)\n' &&
			return 1

		local name="${metrics[0]}"
		local index="${metrics[1]/ /}"
		local temperature="${metrics[2]}"
		local power_draw="${metrics[3]}"

		case $temperature in
		[0-1][0-9])
			icon="\e[1;32m  \e[0m"
			;;
		[2-3]*)
			icon="\e[1;32m  \e[0m"
			;;
		[4-5]*)
			icon="\e[1;32m  \e[0m"
			;;
		[6-7]*)
			icon="\e[1;33m  \e[0m"
			;;
		*)
			icon="\e[1;31m  \e[0m"
			;;
		esac

		printf "%s (%s): $icon %.2f°C,  %.2fW\n" "$name" "$index" "$temperature" "$power_draw"

		return
	fi

	# echo -e '\e[1;32m GPU \e[0m'
	printf 'GPU unavailable\n'
	return 1
} # End nvidia_temp

thunderbolt_reset() {
	sudo tee /sys/bus/wmi/devices/86CCFD48-205E-4A77-9C48-2021CBEDE341/force_power <<<$(echo 1)
	sudo tee /sys/bus/pci/rescan <<<$(echo 1)
} # End thunderbolt_reset

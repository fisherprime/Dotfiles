#
# ~/.local/functions/system-maintenance.sh
#

update_initramfs() {
	# Script to regenerate the initramfs' for the installed kernels with dracut.

	[[ ! -x "$(command -v dracut-install.sh)" ]] &&
		printf '"/usr/local/bin/dracut-install.sh" does not exist\n' &&
		return 1

	sudo -v

	for kernel_pkgbase in /usr/lib/modules/*/pkgbase; do
		echo "$kernel_pkgbase" | sed -E 's/\/(.*)/\1/' | sudo dracut-install.sh &
	done
	wait
}

python_upgrade() {
	# Update Python packages.

	printf "[*] Upgrading Python packages\n"
	[[ -x $(command -v pipx) ]] && pipx upgrade-all

	return 0
} # End python_upgrade

vim_update() {
	# Update Vim plugins.

	[[ -x $(command -v vim) ]] &&
		printf "[*] Updating vim plugins\n" &&
		vim -c ":PlugUpgrade | :PlugUpdate | : CocUpdate"

	return 0
} # End vim_update

if [[ -x /usr/bin/pacman ]]; then
	# Define pacman maintenance functions.

	# NOTE: Alter fuzzy finders array to contain those installed in order of preference.
	fuzzy_finders=("sk" "fzf")
	for fuzzy in "${fuzzy_finders[@]}"; do
		# REF: https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Browsing_packages
		if [[ -x $(command -v "$fuzzy") ]]; then
			pacman_browse() {
				\pacman -Qq | $fuzzy --preview '\pacman -Qil {}' \
					--layout=reverse --bind 'enter:execute(\pacman -Qil {} | less)'
			} # End pacman_browse

			break
		fi
	done

	pacman_remove_orphans() {
		# Recursively remove orphaned packages and their configuration files.

		printf "[!] Removing unused packages\n"
		# shellcheck disable=SC2046
		pacman -Rns $(\pacman -Qtdq)

		return 0
	} # End pacman_remove_orphans

	pacman_remove_old_cache() {
		printf "[!] Removing old cache packages\n"
		pacman -Sc

		return 0
	} # End pacman_remove_old_cache

	pacman_clear_cache() {
		printf "[!] Clearing the cache directory\n"
		pacman -Scc

		return 0
	} # End pacman_clear_cache

	pacman_reinstall_all() {
		printf "[!] Reinstalling all packages\n"
		pacman -Qqn | pacman -S -

		return 0
	} # End pacman_reinstall_all

	pacman_rankmirrors() {
		export MIRRORLIST=/etc/pacman.d/mirrorlist
		export MIRRORLIST_SOURCE="https://archlinux.org/mirrorlist/all/"
		export RANKED_MIRRORLIST="/tmp/mirrorlist.ranked"
		export RANKED_MIRRORLIST_SIZE=30
		export TEMP_FILE="/tmp/mirrorlist.tmp"

		printf "[+] Ranking the pacman mirrorlist\n"

		sudo -v

		# Cache the mirrorlist from archlinux's web resource into a temporary mirrorlist file.
		# Uncomment all server entries for ranking.
		curl -s "$MIRRORLIST_SOURCE" | sed "s/#Server/Server/" >"$TEMP_FILE"

		if [[ -r "$TEMP_FILE" ]]; then
			# The rankmirrors script takes long to complete, rather have it run elevated avoiding a
			# missed sudo authentication event.

			# shellcheck disable=SC1004
			sudo \
				--preserve-env=RANKED_MIRRORLIST_SIZE,TEMP_FILE,RANKED_MIRRORLIST,MIRRORLIST \
				sh -c "
				rankmirrors -n $RANKED_MIRRORLIST_SIZE -v $TEMP_FILE >$RANKED_MIRRORLIST &&
					cp $RANKED_MIRRORLIST $MIRRORLIST
				rm -f $RANKED_MIRRORLIST $TEMP_FILE"
		fi

		unset MIRRORLIST
		unset MIRRORLIST_SOURCE
		unset RANKED_MIRRORLIST
		unset RANKED_MIRRORLIST_SIZE
		unset TEMP_FILE
	} # End pacman_rankmirrors

	pacman_maintenance() {
		local command_name
		command_name="$(basename "$0")"

		local pkgOpt
		local rankOpt
		local pacman_lock_file=/var/lib/pacman/db.lck

		printf "[*] Running Pacman maintenance tasks\n"

		if [[ $(($(pgrep -c pacman) + 0)) -eq 0 && -e $pacman_lock_file ]]; then
			sudo rm ${pacman_lock_file:?}
		elif [[ $(($(pgrep -c pacman) + 0)) -gt 0 ]]; then
			printf "[!] Another pacman process is running\n"
			log_w "$command_name exit with status: Pacman already running"

			return 1
		fi

		printf "[+] Updating the pacman keybase\n"
		sudo pacman-key -u

		printf "[?] Remove unnecessary packages\n"
		printf "y/N: "
		read -r pkgOpt

		case $pkgOpt in
		Y | y)
			pacman_remove_orphans
			pacman_remove_old_cache
			;;
		N | n)
			printf "[+] Skipping package removal\n"
			;;
		*)
			printf "[!] Invalid choice, skipping package removal\n"
			;;
		esac

		printf "[+] Ranking the Pacman mirrorlist
[?] It may take a while, proceed
[y]es/[R]eplace/[N]o: "
		read -r rankOpt

		mirrorlist=/etc/pacman.d/mirrorlist
		mirrorlist_backup=/etc/pacman.d/mirrorlist.backup
		mirrorlist_source="https://www.archlinux.org/mirrorlist/all/"
		temp_file="/tmp/mirrorlist.tmp"

		# Backup the mirrorlist, if one doesn't exist.
		[[ ! -e $mirrorlist_backup ]] &&
			sudo cp "$mirrorlist" "$mirrorlist_backup"

		case $rankOpt in
		Y | y)
			pacman_rankmirrors
			;;
		R | r)
			printf "[+] Replacing mirrorlist with retrieved version\n"

			curl -o "$temp_file" "$mirrorlist_source" &&
				sudo cp "$temp_file" "$mirrorlist" &&
				rm -f "$temp_file"
			;;
		N | n)
			printf "[+] Skipping mirrorlist ranking\n"
			;;
		*)
			printf "[!] Invalid choice, exiting...\n"
			;;
		esac

		return 0
	} # End pacman_maintenance
fi # End pacman stuff

sys_update() {
	_update_gpg_keys() {
		[[ -x $(command -v gpg) ]] &&
			printf "[+] Fetching GPG keys\n" &&
			(gpg --fetch-keys &)
	}

	_update_asp() {
		# Update ASP package sources.

		[[ -x $(command -v asp) ]] &&
			printf "[+] Updating ASP build sources\n" &&
			asp update
	}

	_update_clamav() {
		# Update clamav signatures database.
		# Useful only when the freshclam service isn't being used.

		[[ -x /bin/freshclam ]] &&
			printf "[+] Updating clamav signatures database\n" &&
			sudo freshclam
	}

	_update_r() {
		[[ -x /bin/Rscript ]] &&
			printf "[+] Updating R packages\n" &&
			Rscript -e "update.packages(); y"
	}

	_update_perl() {
		if [[ -x $(command -v cpan) ]]; then
			[[ -x $(command -v cpanm) ]] && cpanm -u && return 0
			cpan -u
		fi
	}

	_update_texlive() {
		if [[ -x $(command -v tlmgr) ]]; then
			printf "[+] Updating TexLive packages\n"
			tlmgr update --all --self
		fi
	}

	_update_sys_packages() {
		# Update system packages, with pacman.
		# Not executing in background as it requires user input.

		[[ -x /bin/pacman ]] &&
			printf "[+] Updating system packages\n" &&
			pacman -Syu
	}

	printf "[*] Running system update tasks\n"

	sudo -v

	_update_gpg_keys

	_update_asp

	# (_update_clamav &)

	# $ZPREZTODIR tends to be $HOME/.zprezto
	[[ -d $ZPREZTODIR ]] && (zprezto-update &)

	# Update R packages
	# (_update_r &)

	(python_upgrade &)

	# (_update_perl &)

	_update_texlive

	wait

	_update_sys_packages

	vim_update

	return 0
} # End sys_update

sys_audit() {
	_lynis_task() {
		# System & security audit.

		printf "[+] Performing Lynis system audit\n"
		sudo lynis audit system
	}

	_rkhunter_task() {
		# Check for rootkits.

		printf "[+] Hunting for rootkits with RKHunter\n"
		sudo rkhunter -c --sk --update
	}

	_arch_audit_task() {
		# Show vulnerable pacman packages (with CVEs).

		printf "[+] Checking packages against Arch's CVEs\n"
		arch-audit -rc
	}

	printf "[*] Performing a system audit\n"

	_lynis_task
	_rkhunter_task
	_arch_audit_task

	return 0
} # End sys_audit

acl_backup() {
	# Backup Access Control Lists.

	printf "[*] Backing up --file-- Access Control Lists\n"

	local acl_backup_dir="/var/local/acl-backup"

	# List broken symlinks
	# find -xtype l -print

	[[ ! -d "$acl_backup_dir" ]] && sudo mkdir -p "$acl_backup_dir"

	# NOTE: Yields a file with the permission: 0600.
	# sudo getfacl -R /boot /etc /usr | sudo cp /dev/stdin "$acl_backup_dir/backup-$(date +%Y-%m-%d\(%H:%M\)).acl"

	sudo getfacl -R /boot /etc /usr |
		sudo tee "$acl_backup_dir/backup-$(date +%Y-%m-%d\(%H:%M\)).acl" >/dev/null

	return 0
} # End acl_backup

train_spamassassin() {
	# Train SpamAssassin.

	_sa_learn_spam() {
		local s_dir="$1"
		[[ -d "$s_dir" ]] &&
			sa-learn --spam --progress "$s_dir"
	}

	_sa_learn_ham() {
		local h_dir="$1"
		[[ -d "$h_dir" ]] &&
			sa-learn --ham --progress "$h_dir"
	}

	printf "[*] Training SpamAssassin's filter\n"

	# Update mailboxes.
	# [[ -x $(command -v mbsync) ]] && mbsync -a

	for mail_dir in "$HOME"/.mail/*; do
		[[ -d "$mail_dir" ]] || continue

		local gmail_spam_dir="$mail_dir/\[Gmail\]/Spam"
		local generic_spam_dir="$mail_dir/Spam"
		# local gmail_ham_dir="$mail_dir/\[Gmail\]/Ham"
		# local generic_ham_dir="$mail_dir/Ham"

		if [[ "$mail_dir" == *"gmail"* || "$mail_dir" == *"GMAIL"* ]]; then
			_sa_learn_spam "$gmail_spam_dir"
			# _sa_learn_ham "$gmail_ham_dir"
		else
			_sa_learn_spam "$generic_spam_dir"
			# _sa_learn_ham "$generic_ham_dir"
		fi
	done

	return 0
} # End train_spamassassin

misc_cleanup() {
	_cleanup_wget_logs() {
		locate wget-lo | sudo xargs rm
	}

	_cleanup_zsh_dumps() {
		locate .zcompdu | sudo xargs rm
	}

	_cleanup_viminfo() {
		locate .viminfo | xargs rm
	}

	sudo updatedb

	_cleanup_wget_logs &
	_cleanup_zsh_dumps &
	# _cleanup_viminfo
	wait

	return 0
} # End misc_cleanup

sys_maintenance() {
	printf "[*] Performing maintenance tasks\n"

	sudo -v

	acl_backup &
	sleep 2

	train_spamassassin
	sys_audit

	return 0
} # End sys_maintenance

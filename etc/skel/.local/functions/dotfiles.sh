#
# ~/.local/functions/dotfiles.sh
#

dots_update() {
	# Update Dotfiles.

	# Rsync options:
	# Recursively updating destination "-ru"
	# Delete excluded files from the destination "--delete-excluded"
	# Delete files missing from the source "--delete-missing-args".

	[[ -z $GIT_HOME ]] &&
		printf "GIT_HOME is not exported, returning from function\n" &&
		return 1

	local command_name
	command_name="$(basename "$0")"

	local command_flags
	local arch_dots_dir="$GIT_HOME/dotfiles-arch"
	if [[ ! -d $arch_dots_dir ]]; then
		mkdir -p "$arch_dots_dir" ||
			return "$(dir_not_exist "$arch_dots_dir")"
	fi

	local rsync_exclude_file="$HOME/rsync-exclude.txt"
	local rsync_include_file="$HOME/rsync-include.txt"

	_usage() {

		printf "[+] Usage: %s [options]

Options:
	-h,--help:               Print this help message
	-B,--both:               Update home and global dotfiles
	-H,--home:               Update home dir dotfiles
	-S,--sys:                Update global dotfiles
" "$command_name"

	} # End _usage

	if [[ $# -gt 2 ]]; then
		_usage
		log_w "$command_name exit with status: Invalid parameters"

		return 1
	fi

	local options
	# shellcheck disable=SC2140
	options=$(getopt -o hBHS -l "help","both","home","sys" -n "$command_name" -- "$@")

	# shellcheck disable=SC2181
	if [[ $? -ne 0 ]]; then
		printf "[!] Failed parsing command options\n"
		_usage

		return 1
	fi

	eval set -- "$options" # Don't know what this does

	while [[ $1 ]]; do
		case $1 in
		-h | --help)
			_usage
			log_i "$command_name exit with status: Success"

			return 0
			;;
		-B | --both)
			command_flags+=".-B"
			;;
		-H | --home)
			command_flags+=".-H"
			;;
		-S | --sys)
			command_flags+=".-S"
			;;
		--)
			break
			;;
		*)
			_usage
			log_w "$command_name exit with status: Invalid parameters"

			return 1
			;;
		esac
		shift
	done

	_list_modified_package_files() {
		pacman -Qii | awk '/^MODIFIED/ {print $2}'
	}

	_pkg_list_upd() {
		local arch_packagelist_dir="$1/packages"
		if [[ ! -d $arch_packagelist_dir ]]; then
			mkdir "$arch_packagelist_dir" ||
				return "$(dir_not_exist "$arch_packagelist_dir")"
		fi

		printf "[+] Updating package lists(pacman, pip, pip2)\n"

		# Pacman packages list
		\pacman -Qqe >"$arch_packagelist_dir/packages.txt"
		\pacman -Qqen >"$arch_packagelist_dir/pacman.txt"

		# Python3 package list.
		[[ -x $(command -v pip) ]] &&
			(
				\pip freeze --local --user | grep -v '^\-e' | cut -d = -f 1 \
					>"$arch_packagelist_dir/pip3.txt" &
			)

		# Pipx package list.
		[[ -x $(command -v pipx) ]] &&
			(\pipx list | tail -n +3 >"$arch_packagelist_dir/pipx.txt" &)

		wait
	} # End _pkg_list_upd

	_home_upd() {
		# Save HOMEDIR dots

		printf "[+] Updating homedir dots\n"

		rsync -ruL --existing --include-from="$rsync_include_file" \
			--exclude-from="$rsync_exclude_file" --delete-excluded \
			--progress "$HOME/" "$arch_dots_dir/home/"
	} # End _home_upd

	_sys_upd() {
		# Save /etc (System) dots

		printf "[+] Updating system dots\n"

		# Exclude formats:
		# --exclude={1,2}
		# --exclude=1 --exclude=2.

		# System configs
		sudo rsync -ruk --existing --progress /etc/ \
			"$arch_dots_dir/etc"

		# `/usr/local` stuff
		sudo rsync -ruk --exclude-from="$rsync_exclude_file" \
			--delete-excluded --existing --progress /usr/local/ \
			"$arch_dots_dir/usr/local"

		# `/usr/lib` stuff
		sudo rsync -ruk --exclude-from="$rsync_exclude_file" \
			--delete-excluded --existing --progress /usr/lib/ \
			"$arch_dots_dir/usr/lib"

		sudo chown -R "$(whoami):$(whoami)" "$arch_dots_dir/etc" \
			"$arch_dots_dir/usr"
	}

	# End _sys_upd

	_generalize_data() {
		# Rather have these private

		# SSH daemon config
		sed -i -E 's/^Port.*/#Port/' "$arch_dots_dir/etc/ssh/sshd_config"

		# GRUB config
		# local crypt_replace='s/(CMDLINE_LINUX=.*cryptdevice=UUID=)\S*/\1<cryptdevice-uuid:name>/'
		# local root_replace='s/(CMDLINE_LINUX=.*root=)\S*/\1<\/dev\/xxx>/'
		# local swap_replace='s/(CMDLINE_LINUX=.*resume=)\S*/\1<\/dev\/xxx>/'
		local crypto_replace='s/(CMDLINE_LINUX=.*crypto=)\S*/\1<hash>:<cipher>:<keysize>:<offset>:<skip>/'

		# Debug
		# echo sed -i -E \
		# "$crypt_replace;$root_replace;$swap_replace;$crypto_replace" \
		# "$dotfiles_repo/Dotfiles-Arch-Optimus/etc/default/grub"

		sed -i -E "$crypto_replace" \
			"$arch_dots_dir/etc/default/grub"

		# GRUB custom config
		# sed -i -E 's/^set superusers.*$/set superusers="<username>"/; s/^password_pbkdf2.*/password_pbkdf2 <username> <pbkdf2 key>/' "$dotfiles_repo"/Dotfiles-Arch-Optimus/etc/grub.d/40*
	} # End _generalize_data

	_backup_dots() {
		# Synchronize with other backup solutions

		# Array of paths to the local end of backup solutions:
		# "$GDRIVE_DIR"
		# "$DROPBOX_DIR".
		local backup_locations=()

		for loc in "${backup_locations[@]}"; do
			[[ -d $loc ]] &&
				rsync -ruk --exclude-from="$rsync_exclude_file" \
					--delete-excluded --delete-missing-args --progress \
					"$arch_dots_dir" "$loc" &
		done

		wait
	} # End _backup_dots

	printf "[*] Updating dotfiles\n"

	_pkg_list_upd "$arch_dots_dir"

	while [[ $command_flags != "" ]]; do
		case $command_flags in
		.-B)
			# Require elevated permissions
			sudo -v || return

			_home_upd &
			_sys_upd &
			wait

			command_flags="$(echo "$command_flags" | sed -E 's/\.-B//')"
			;;
		.-H)
			_home_upd

			command_flags="$(echo "$command_flags" | sed -E 's/\.-H//')"
			;;
		.-S)
			sudo -v || return
			_sys_upd

			command_flags="$(echo "$command_flags" | sed -E 's/\.-S//')"
			;;
		*)
			printf "[!] Invalid choice\n"
			log_w "$command_name exit with status: Invalid parameters"

			return 1
			;;
		esac
	done

	_generalize_data
	_backup_dots

	cd "$arch_dots_dir" || {
		log_w "$command_name exit with status: Change directory failure"
		return 1
	}
	chown -Rf "$USER:$USER" ./* # Change permissions for non-root files

	git status -s
	# git -C "$dotfiles_repo" status -s # Alternatively

	return 0
} # End dots_update

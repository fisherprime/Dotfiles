#
# ~/.local/functions/cpu-preformance.sh
#

list_smt_cpus() {
	get_smt_cpus &&
		printf "SMT CPU(s): %s\n" "${smt_cpus[*]}"
}

get_smt_cpus() {
	# Get a list of hyper threaded CPU cores
	smt_cpus=()
	export SMT_CPUS=""

	for processor in /sys/devices/system/cpu/cpu[0-9]*; do
		# NOTE: unused as I have left some debugging directive on somewhere.
		# Rebinding the below variables; new values pollute `stdout`.
		# local cpu_id
		# local core_id

		cpu_id="$(basename "$processor" | sed -E 's/[a-z]+//')"

		[[ -r "$processor/topology/core_id" ]] &&
			core_id=$(cat "$processor/topology/core_id")

		[[ -z $core_id || $cpu_id != "$core_id" ]] &&
			smt_cpus+=("$cpu_id") &&
			append_export "SMT_CPUS" "$cpu_id"

	done

	[[ ${#smt_cpus[*]} -lt 1 ]] && return 1

	export SMT_STATE=1
	# Debug
	# printf "SMT CPU(s): %s\n" "${smt_cpus[*]}"

} # End export_smt_cpu

alter_smt_state() {
	option="$1"

	sudo -v

	get_smt_cpus
	[[ ${#smt_cpus[*]} -lt 1 ]] && return

	for cpu_id in "${smt_cpus[@]}"; do
		if [[ -d "/sys/devices/system/cpu/cpu$cpu_id" ]]; then
			cpu_path="/sys/devices/system/cpu/cpu$cpu_id"

			[[ -r "$cpu_path/topology/core_id" ]] &&
				core_id="$(cat "$cpu_path/topology/core_id")"

			case "$option" in
			-e | --enable)
				if [[ -r "$cpu_path/online" ]]; then
					printf "[+] CPU: %s -> enable\n" "$cpu_id"

					export CPU_STATE_FILE="$cpu_path/online"
					sudo --preserve-env=CPU_STATE_FILE sh -c 'echo 1 > $CPU_STATE_FILE'
				fi
				;;
			-d | --disable)

				if [[ -r "$cpu_path/online" ]]; then
					printf "[+] Core: %s, CPU: %s -> disable\n" "$core_id" "$cpu_id"

					export CPU_STATE_FILE="$cpu_path/online"
					sudo --preserve-env=CPU_STATE_FILE sh -c 'echo 0 > $CPU_STATE_FILE'
				fi

				;;
			*) ;;
			esac
		fi
	done

}

set_smt_state() {
	# NOTE: This operation will disable/enable cores without regard to their current state; the
	# check is an unnecessary operation.

	local command_name
	command_name="$(basename "$0")"

	_usage() {
		printf "[+] Usage: %s [options]

Options:
	-h,--help:               Print this help message
	-d,--disable             Disable CPU hyper threading
	-e,--enable              Enable CPU hyper threading
" "$command_name"

	} # End _usage

	if [[ $# -ne 1 ]]; then
		_usage
		logger "$command_name exit with status: Invalid parameters"
		return 1
	fi

	local options
	# shellcheck disable=SC2140
	options=$(getopt -o hed -l "help","enable","disable" -n "$command_name" -- "$@")

	# shellcheck disable=SC2181
	if [[ $? -ne 0 ]]; then
		printf '[!] Failed parsing command options\n'
		_usage
		return 1
	fi
	eval set -- "$options" # Don't know what this does

	while [[ $1 ]]; do
		case $1 in
		-h | --help)
			_usage
			logger "$command_name exit with status: Success"
			return 0
			;;
		-e | --enable)
			printf '[*] Enabling CPU hyper threading\n'

			alter_smt_state "$1"
			export SMT_STATE=1
			;;
		-d | --disable)
			printf '[*] Disabling CPU hyper threading\n'

			alter_smt_state "$1"
			export SMT_STATE=0
			;;
		--)
			break
			;;
		*)
			_usage
			logger "$command_name exit with status: Invalid parameters"
			return 1
			;;
		esac
		shift
	done

} # End set_smt_state

# NOTE: Better off using GNU parallel.
multithread() {
	# local command_name
	# command_name="$(basename "$0")"

	local commands_list

	while [[ $1 ]]; do
		commands_list+=("$1")
		shift
	done

	printf "[*] Multithreading command
: %s\n" "${commands_list[*]}"

	# Debug.
	# printf "Command list: %s\n" "${commands_list[*]}"

	# Word splitting is necessary for this to work.
	# shellcheck disable=SC2086
	chrt -r 1 taskset -c 0-8 ${commands_list[*]}

	return 0
} # End multithread

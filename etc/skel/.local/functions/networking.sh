#
# ~/.local/functions/networking.sh
#

my_ip() {
	# Print current global IP

	printf '[*] Obtaining current global IP\n'
	curl -s checkip.dyndns.org | sed -E 's/.*Current IP Address: //; s/<.*$//'
} # End my_ip

rand_eth() {
	# Randomize Ethernet interface MAC

	printf '[*] Randomizing Ethernet interface MAC\n'

	export IFACE_NAME
	export IFACE_STATE

	if [[ $1 -gt 1 && $1 -le ${#ETH_IFACES[*]} ]]; then
		IFACE_NAME="${ETH_IFACES["$((ARRAY_START + $1))"]}"
	else
		IFACE_NAME="$ETH_IFACE"
	fi

	# Need better implementation
	IFACE_STATE="$(ip link show "$IFACE_NAME" | sed -E \
		"s/.*state\ //; s/\ .*//" | tr "[:upper:]" "[:lower:]")"

	# shellcheck disable=SC1004
	sudo --preserve-env=IFACE_NAME,IFACE_STATE sh \
		-c 'ip link set dev "$IFACE_NAME" down; \
		macchanger -e "$IFACE_NAME"; \
		ip link set "$IFACE_NAME" $IFACE_STATE'

	unset IFACE_NAME
	unset IFACE_STATE

	return 0
} # End rand_eth

unrand_eth() {
	# Reset Ethernet interface MAC to its default

	printf '[*] Ethernet interface MAC reverted\n'

	export IFACE_NAME
	export IFACE_STATE

	if [[ $1 -gt 1 && $1 -le ${#ETH_IFACES[*]} ]]; then
		IFACE_NAME="${ETH_IFACES["$((ARRAY_START + $1))"]}"
	else
		IFACE_NAME="$ETH_IFACE"
	fi

	# Need better implementation
	IFACE_STATE="$(ip link show "$IFACE_NAME" | sed -E \
		"s/.*state\ //; s/\ .*//" | tr "[:upper:]" "[:lower:]")"

	# shellcheck disable=SC1004
	sudo --preserve-env=IFACE_NAME,IFACE_STATE sh -c ' \
		ip link set dev "$IFACE_NAME" down; \
		macchanger -p "$IFACE_NAME"; \
		ip link set "$IFACE_NAME" $IFACE_STATE'

	unset IFACE_NAME
	unset IFACE_STATE

	return 0
} # End unrand_eth

_hotspot_cleanup() {
	# Perform clean-up operations for hotspot_on.
	# Needs handle (internal number that identifies a rule) to delete nftables rules.

	printf '[+] Performing clean-up on NATed wireless access point\n'

	if [[ -z $AP_IFACE ]]; then
		export AP_IFACE='wlan_ap'
		# export STD_IFACE='wlan_std'
	fi

	export DNS_REDIRECT_RULE_HANDLE
	export INET_FWD_RULE_1_HANDLE
	export INET_FWD_RULE_HANDLE
	export NAT_RULE_HANDLE

	printf "[+] Elevated permissions required\n"

	# Forward NAT to Internet rule handle.
	INET_FWD_RULE_HANDLE="$(sudo nft -ann list ruleset | grep -E "iifname \"$AP_IFACE\" oifname" | sed -E "s/.*\# handle //")"

	# NAT postrouting rule handle, provides masqueraded IPs.
	NAT_RULE_HANDLE="$(sudo nft -ann list ruleset | grep -E ".*Masquerade wlan_ap" | sed -E "s/.*\# handle //")"

	# Debug.
	# printf "DNS: %s, INET: %s, INET_1: %s, NAT: %s\n" "$DNS_REDIRECT_RULE_HANDLE" "$INET_FWD_RULE_HANDLE" "$INET_FWD_RULE_1_HANDLE" "$NAT_RULE_HANDLE"

	get_network_iface

	# shellcheck disable=SC1004
	sudo --preserve-env=AP_IFACE,NET_IFACE,AP_NETWORK,STD_IFACE,INET_FWD_RULE_HANDLE,NAT_RULE_HANDLE sh -c ' \
		rmmod nf_nat_pptp; \
		if [[ "$(nft list ruleset)" == *"iifname \"$AP_IFACE\""* ]]; then \
			nft delete rule inet filter forward handle $INET_FWD_RULE_HANDLE; \
			nft delete rule nat postrouting handle $NAT_RULE_HANDLE; \
		fi; \
		 \
		pkill hostapd; \
		systemctl stop dnsmasq.service; \
		 \
		if [[ "$(ip link)" == *"$AP_IFACE"* ]]; then \
			sysctl net.ipv4.conf.all.rp_filter=1; \
			sysctl net.ipv4.conf.default.rp_filter=1; \
			sysctl net.ipv4.conf."$NET_IFACE".forwarding=0; \
			sysctl net.ipv4.ip_forward=0; \
			 \
			sleep 1; \
			 \
			ip link set dev "$AP_IFACE" down; \
			ip addr flush "$AP_IFACE"; \
			iw dev "$AP_IFACE" del; \
		fi; \
		printf "[+] Done\n"'

	# Unset trap
	trap - SIGINT SIGTERM SIGQUIT SIGABRT EXIT

	return 0
} # End _hotspot_cleanup

hotspot_on() {
	# Enable a Wi-Fi access point|Hotspot.
	# Not using hostapd.service to have verbose output in terminal.

	printf '[*] Starting up NATed wireless access point\n'

	sudo -v

	get_network_iface

	[[ -z $AP_MAC_ADDR || -z $STD_MAC_ADDR ]] && setup_wifi_ifaces
	printf "[MACs] AP: %s, STD: %s\n" "$AP_MAC_ADDR" "$STD_MAC_ADDR"

	export FORWARD_INSERT_POS
	FORWARD_INSERT_POS="$(sudo nft -ann list ruleset | grep -E "Drop unknown forward traffic \(forward-INET\)" | sed -E "s/.*\# handle //")"

	# Cleanup on Interrupt.
	# SIGKILL|SIGSTOP cannot be trapped; also, don't trap SIGTERM.
	trap "_hotspot_cleanup" SIGINT SIGQUIT SIGABRT

	get_outbound_iface

	# shellcheck disable=SC1004
	# Using stdbuf to line buffer output, immediately display output.
	# TODO: Move this to a script.
	sudo --preserve-env=AP_IFACE,AP_MAC_ADDR,AP_IP,AP_NETWORK,AP_BROADCAST,STD_IFACE,STD_MAC_ADDR,NET_IFACE,WIFI_IFACE,FORWARD_INSERT_POS sh -c ' \
		iw dev "$WIFI_IFACE" interface add "$AP_IFACE" type managed addr "$AP_MAC_ADDR"; \
		sleep 1; \
		 \
		rfkill unblock wlan; \
		ip link set dev "$AP_IFACE" up; \
		ip addr add dev "$AP_IFACE" "$AP_IP"/24; \
		sleep 1; \
		 \
		systemctl start dnsmasq.service; \
		stdbuf -oL hostapd /etc/hostapd/hostapd.conf& \
		 \
		sysctl net.ipv4.ip_forward=1; \
		sysctl net.ipv4.conf."$NET_IFACE".forwarding=1; \
		sysctl net.ipv4.conf.all.rp_filter=2; \
		sysctl net.ipv4.conf.default.rp_filter=2; \
		 \
		nft insert rule inet filter forward position $FORWARD_INSERT_POS iifname $AP_IFACE oifname $NET_IFACE counter accept comment \""Accept wlan_ap (forward-INET)"\"; \
		nft add rule inet nat postrouting ip saddr $AP_NETWORK oifname $NET_IFACE counter masquerade random, persistent comment \""Masquerade wlan_ap (postrouting-NAT)"\"; \
		modprobe nf_nat_pptp'

	# sudo rfkill unblock wifi; sudo create_ap -c 1 "$WIFI_IFACE" "$ETH_IFACE" AdminPC Velkomen --no-virt

	return 0
} # End hotspot_on

hotspot_restart() {
	pkill hotspot_on && hotspot_on
} # End hotspot_restart

setup_debug_proxy() {
	# Set up system debug proxy

	local proxy_host="http://192.168.137.1"
	local proxy_port="8888"

	printf '[*] Setting up proxy connection\n'

	export http_proxy="$proxy_host:$proxy_port"
	export https_proxy="$proxy_host:$proxy_port"
	export ftp_proxy="$proxy_host:$proxy_port"
	export rsync_proxy="$proxy_host:$proxy_port"
	export rvm_proxy="$proxy_host:$proxy_port"
	export no_proxy="localhost, arch.localnet"

	return 0
} # End setup_debug_proxy

unset_debug_proxy() {
	# Unset system debug proxy

	printf '[*] Dismantling proxy connection\n'

	unset rvm_proxy
	unset no_proxy
	unset rsync_proxy
	unset ftp_proxy
	unset https_proxy
	unset http_proxy

	return 0
} # End unset_debug_proxy

hosts_update() {
	# Update Hosts file, using StevenBlack's hosts generator

	local command_name
	command_name="$(basename "$0")"

	[[ ! -x $(command -v hostsgen) ]] && return

	printf '[*] Updating hosts file\n'

	hostsgen && sudo cp /tmp/hosts /etc/hosts
	if [[ -d $GIT_HOME ]]; then
		grep -v localnet /tmp/hosts >"$GIT_HOME/Block/hosts"

		cd "$GIT_HOME/Block" || {
			log_w "$command_name exit with status: Change directory failure"
			return 1
		}

		# Remove comments
		grep -E '^0' hosts >hosts-Min

		# Remove comments & redirection IPs
		sed -E 's/^0.0.0.0 //; s/^#.*//; s/.*\s.*//; s/^[0-9].*\..*//; /^\s*$/d' hosts >hosts-Only

		git add . && git commit -m "perf(*): Update hosts files" && git push
	fi
	rm /tmp/hosts

	return 0
} # End hosts_update

dnscrypt_test() {
	# Test DNSCrypt security status with either "unbound-host" or drill

	printf '[*] Checking DNSCrypt status\n'

	if [[ -e /bin/unbound ]]; then
		# shellcheck disable=SC1004
		sudo sh -c 'printf "[+] Testing SECURE & BOGUS status(es)\n"; \
			unbound-host -C /etc/unbound/unbound.conf -v sigok.verteiltesysteme.net& \
			unbound-host -C /etc/unbound/unbound.conf -v sigfail.verteiltesysteme.net& \
			wait'

		return 0
	fi

	# NOTE: Executing these queries in the background yields a messy result on query failure.
	printf "[+] Testing SECURE status\n"
	drill -V 5 sigok.verteiltesysteme.net

	printf "\n[+] Testing BOGUS status\n"
	drill -V 5 sigfail.verteiltesysteme.net

	# (drill -V 5 sigok.verteiltesysteme.net &
	# (drill -V 5 sigfail.verteiltesysteme.net &)
	# wait

	# NOTE: Requires the `parallel` executable
	# {
	# echo sigok.verteiltesysteme.net
	# echo sigfail.verteiltesysteme.net
	# } | parallel -k drill -V 5

	return 0
} # End dnscrypt_test

#
# ~/.local/functions/network-iface-setup.sh
#

# NOTE: Some `eval` magic for dynamic variable updates.
append_iface() {
	local value="$2"
	local var="$1"

	[[ -z $var || -z $value ]] && return

	# Debug
	# eval "echo ${var}"

	# Debug
	# printf "%s: %s, val: %s\n" "$1" "$(eval "echo \$$var")" "$value"

	case ":$(eval "echo \$$var"):" in
	*:$value:*) ;;
	*)
		eval "export $var=\"\${$var:+\$$var:}\$value\""
		;;
	esac
}

append_eth_iface() {
	append_iface "ETH_IFACES" "$1"
}

append_wifi_iface() {
	append_iface "WIFI_IFACES" "$1"
}

export_eth_ifaces() {
	# Export arrays of Ethernet network interface names & set the default ethernet interface.

	export ETH_IFACE
	export ETH_IFACES

	local default_set=0

	for iface_path in /sys/class/net/en*; do
		[[ -e "$iface_path" ]] || continue

		iface="$(basename "$iface_path")"
		append_eth_iface "$iface"

		[[ $iface == *"enp"* && $default_set -eq 0 ]] &&
			ETH_IFACE="$iface" && default_set=1
	done
}

export_wifi_ifaces() {
	# Export arrays of Wi-Fi network interface names & set the default Wi-Fi interface.

	export WIFI_IFACE
	export WIFI_IFACES

	local default_set=0

	for iface_path in /sys/class/net/wl*; do
		[[ -e "$iface_path" ]] || continue

		iface="$(basename "$iface_path")"
		append_wifi_iface "$iface"

		[[ $iface != *"wlan_ap"* && $default_set -eq 0 ]] &&
			WIFI_IFACE="$iface" && default_set=1
	done
}

export_ifaces() {
	export_eth_ifaces
	export_wifi_ifaces

	# Debug
	# printf "Eths: %s \nWls: %s \nEth: %s \nWifi: %s\n" "${ETH_IFACES[*]}" \
	# "${WIFI_IFACES[*]}" "$ETH_IFACE" "$WIFI_IFACE"
} # End export_iface

setup_wifi_ifaces() {
	# Export names & MAC addresses for virtual Wi-Fi interfaces.
	# In preparation for Wi-Fi hotspot setup.

	export AP_IFACE
	export AP_BROADCAST
	export AP_MAC_ADDR

	#export STD_IFACE
	export STD_MAC_ADDR

	export AP_NETWORK
	export AP_IP

	# STD_IFACE='wlan_std'
	AP_IFACE='wlan_ap'

	# Get MAC without macchanger
	# ip link show "$WIFI_IFACE" | grep link | sed -E 's/^    \S* //; s/ .*$//'

	local def_mac_prefix
	def_mac_prefix="$(ip link show dev "$WIFI_IFACE" | grep -i link/ether | sed -E 's/^.*ether //; s/ .*//; s/:..:..:..$//')"

	# This is the software access point interface
	AP_MAC_ADDR="$(printf "%s:%02X:%02X:%02X\n" "$def_mac_prefix" "$((RANDOM % 256))" "$((RANDOM % 256))" "$((RANDOM % 256))")"

	# Uncomment the below lines and use the address generated.
	# Recommended option is to set a hard-coded STD_MAC_ADDR otherwise the
	# random MAC address generated on initialization should be allowed for MAC
	# filters.

	# $RANDOM=$RANDOM # Else they get the same MAC
	# STD_MAC_ADDR="$def_mac_prefix"':%02X:%02X:%02X\n' $(( RANDOM % 256 )) $(( RANDOM % 256)) $(( RANDOM % 256 )))

	# Setting a fixed MAC for the Wi-Fi connecting interface
	STD_MAC_ADDR="$def_mac_prefix:0C:1C:E2"

	# sudo macchanger -p "$WIFI_IFACE" # Reset default iface's MAC

	AP_NETWORK="192.168.12.0/24"
	AP_IP="192.168.12.1"
	AP_BROADCAST="192.168.12.255"

	# Register newly added interfaces.
	export_wifi_ifaces
} # End setup_wifi_ifaces

setup_ifaces() {
	export_ifaces
	setup_wifi_ifaces
}

get_outbound_iface() {
	_iface_test() {
		local iface="$1"

		[[ "$(ip link show "$iface" up)" == *"state UP"* ]] &&
			NET_IFACE="$iface" && return 1

		return 0
	}

	export NET_IFACE

	# Interfaces to test in order of preference.
	local ifaces=("$WIFI_IFACE" "$ETH_IFACE")

	for iface in "${ifaces[@]}"; do
		_iface_test "$iface"
		[[ $? -eq 1 ]] && break
	done

	[[ -z $NET_IFACE ]] &&
		printf '[!] All networking interfaces are down\n' &&
		return 0

	printf '[+] Outbound interface: %s\n' "$NET_IFACE"
} # End get_network_iface

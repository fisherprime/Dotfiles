#
# ~/.local/functions/root-persistence.sh
#

check_root() {
	[[ $(id -u) -eq 0 ]] &&
		printf "[!] Run as normal user\n" &&
		return 1

	return 0
} # End check_root

keep_sudo_alive() {
	# NOTE: This function failed to work as expected.

	# Don't keep alive if root.
	[[ $(check_root) -ne 0 ]] && return

	# Update user timestamp without running a command
	sudo -v

	(while true; do
		# Sleep for less than the default sudo timeout (5 min).
		sleep 120

		# Update user's timestamp non-interactively.
		sudo -nv

		# Exit when the current shell is closed.
		kill -0 "$$" 2>/dev/null || exit

		# Exit when the calling process has terminated.
		# kill -0 "$PPID" 2>/dev/null || exit
	done &)
} # End keep_sudo_alive

_kill_sudo_keepalive() {
	# Will kill itself.
	pkill -P $$
} # End kill_sudo_keepalive

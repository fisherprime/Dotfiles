#
# ~/.tmux.conf
#

# ----
# `unbind` & `unbind-key` do the same thing.
# `bind` & `bind-key` do the same thing.
# `set` & `set-option` do the same thing.

# Remap prefix from "C-b" to "C-a"; easier on the hands.
unbind-key C-b
set -g prefix C-a
bind-key C-a send-prefix

# Allow for the use of C-a a to control a nested tmux session.
#
# NOTE: It's a bad idea to nest tmux sessions; requires the `TMUX` environment variable be unset.
bind-key a send-prefix

# Use vi-style keybindings
set -g mode-keys vi
set -g status-keys vi
# set -g status-keys emacs

# Allow for faster keybindound command inputs.
set -s escape-time 0

# Increase scrollback buffer size.
set -g history-limit 50000

# Don't start a login shell; rather, an interactive shell.
set -g default-command "${SHELL}"

# Enable mouse interaction.
set -g mouse on

# Split pane.
unbind-key '"'
unbind-key %
bind-key h split-window -h
bind-key v split-window -v

# Switch pane.
bind-key Left select-pane -L
bind-key Right select-pane -R
bind-key Up select-pane -U
bind-key Down select-pane -D

# Source .tmux.conf (reload config) as suggested in `man tmux`.
bind-key R source-file "$HOME/.config/tmux/tmux.conf"

# Pane border.
# set -g pane-border-fg black
# set -g pane-active-border-fg green

# Status bar.
set -g status-justify left

# Set correct terminal for 256 colour support.
set -g default-terminal "tmux-256color"
# Enable truecolor (24-bit colour) support.
set -ga terminal-overrides ",alacritty:Tc,cool-retro-term:Tc,termux:Tc,*256col*:Tc"

# Enable window titles, with host string.
set -g set-titles on
set -g set-titles-string "#T"

# Has a weird explanation, does good things though.
setw -g aggressive-resize on

# Activity monitoring
setw -g monitor-activity on
# set -g visual-activity on

# focus events enabled for terminals that support them.
set -g focus-events on

# Enable nested tmux sessions, bad idea.
# unset TMUX

# Manange tmux plugins.
set -g @plugin 'tmux-plugins/tpm'

# set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

# Enable tmux-continuum; continuous tmux environment saves.
set -g @continuum-restore 'on'

# Set plugin options
set -g @yank_selection 'primary' # or 'secondary' or 'clipboard'

# Autoinstall tpm
if "test ! -d $HOME/.config/tmux/plugins/tpm" \
	   "run 'git clone https://github.com/tmux-plugins/tpm $HOME/.config/tmux/plugins/tpm && $HOME/.config/tmux/plugins/tpm/bin/install_plugins'"

# Initialize tmux plugin manager.
run -b '$HOME/.config/tmux/plugins/tpm/tpm'
# ----

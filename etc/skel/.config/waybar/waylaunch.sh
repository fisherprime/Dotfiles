#!/bin/bash

launch_waybar() {
	# Set script execution limit in seconds
	local counter
	local limit
	counter=0
	limit=30

	killall -q waybar

	# Wait for waybar to exit
	while (pgrep -x waybar >/dev/null); do
		# echo "counter: $counter, limit: $limit"
		if [[ $counter -eq limit ]]; then
			# Send SIGKILL to waybar
			pkill -9 waybar
			break
		fi

		counter=$((counter + 1))
		sleep 1
	done

	exec waybar
}

launch_waybar

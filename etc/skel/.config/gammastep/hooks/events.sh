#!/bin/sh
case $1 in
period-changed)
	exec notify-send -i gammastep-status-on "Gammastep" "Period changed to $3"
	;;
esac

#
# ~/.config/ncmpcpp/config
#

# --------------------------------------------------
# Directory for storing ncmpcpp related files.
ncmpcpp_directory = "~/.config/ncmpcpp"
# Directory for storing downloaded lyrics.
lyrics_directory  = "~/Music/Lyrics"

# Connection settings
# mpd_host      = "localhost"
mpd_music_dir = "~/Music"
mpd_port      = "6600"
# mpd_crossfade_time = 5

# Playlist
playlist_display_mode    = "columns"
song_columns_list_format = "(7)[white]{l} (20)[green]{a} (41)[cyan]{b} (32)[magenta]{t}"
song_list_format         = " $8%l$9 $3{%a}$9 $7{%b}$9 $6{%t}|{%f}$9"
song_status_format       = "$3{%a}$9 $7{%b}$9 $6%t$9 "

# Now Playing
now_playing_prefix = "$b$3>$9" # $(green)>
now_playing_suffix = "$8$/b"

# Browser
browser_playlist_prefix = "$2 ♥ $9"

# Delays
message_delay_time               = 2
playlist_disable_highlight_delay = 1

# Progress Bar
progressbar_color         = "black"
progressbar_elapsed_color = "green"
progressbar_look          = "━━─"

# Colours
colors_enabled      = "yes"
current_item_prefix = "$r"
current_item_suffix = "$/r"
empty_tag_color     = "red"
main_window_color   = "green"
state_flags_color   = "green"

# Misc
ask_before_clearing_playlists      = "yes"
autocenter_mode                    = "yes"
centered_cursor                    = "yes"
cyclic_scrolling                   = "yes"
discard_colors_if_item_is_selected = "yes"
display_bitrate                    = "yes"
display_remaining_time             = "no"
empty_tag_marker                   = " ## "
header_visibility                  = "no"
mouse_list_scroll_whole_page       = "yes"
mouse_support                      = "yes"
statusbar_visibility               = "yes"
titles_visibility                  = "yes"

# In order to make the music visualizer work with MPD you need to use the fifo output. 
#
# Its format parameter has to be set to 44100:16:1 for mono visualization or 44100:16:2 for stereo
# visualization.
audio_output {
	type		"fifo"
	name		"visualizer"
	path		"/tmp/mpd.fifo"
	format		"48000:16:2"
}

# visualizer_look = "▋▋"
visualizer_data_source   = "/tmp/mpd.fifo"

# If you set format to stereo, you can set the below option 'yes'.
visualizer_in_stereo   = "yes"

visualizer_look        = "º|" # ●▮
visualizer_output_name = "visualizer"

# Types: spectrum|wave|wave_filled|ellipse
visualizer_type = "spectrum"
# visualizer_fps = 60
# --------------------------------------------------

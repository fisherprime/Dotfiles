#
# ~/.profile
#

# Git projects directory.
[[ -z $GIT_HOME && -d $HOME/Git ]] && export GIT_HOME="$HOME/git-repos"
# ----

# Code stuff
# ----
# Allow for ccache usage if available
export USE_CCACHE=1

[[ $USE_CCACHE -eq 1 && -n $CODE_PATH ]] &&
	export PATH=${CODE_PATH}
# ----

# Audio stuff
# ----
# Mute microphone (with ALSA).
[[ -x $(command -v amixer) ]] && amixer sset Mic mute 2>/dev/null
# ----

# Assuming wayland.
export QT_QPA_PLATFORM='wayland;xcb'
export MOZ_ENABLE_WAYLAND=1
export MOZ_WEBRENDER=1
# Allow XWayland clients to launch firefox.
export MOZ_DBUS_REMOTE=1
# Resolve gray windows for Java GUIs on sway.
export _JAVA_AWT_WM_NONREPARENTING=1

[[ -x $(command -v wal) ]] && wal -R

"
" ~/.vim/filetype.vim
"

augroup filetypedetect
	" Identify Mutt compose temporary files as mail.
	autocmd BufRead,BufNewFile *mutt-* setfiletype mail
	autocmd FileType markdown setlocal shiftwidth=2 softtabstop=2 expandtab
augroup END

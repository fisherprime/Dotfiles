#
# /etc/profile.d/custom.sh
#

# Notes
# ----
# One can't use arrays as environment variables; convert them into strings.
# [[ is an improvement on [.
#
# When this file is sourced, no display server/compositor is set up; checking the `DISPLAY`
# export is useless.
# ----

# Misc stuff
# ----
# Check if shell is running interactively; allow login shells only.
# Disable if running a login manager.
# [[ $- != *i* ]] && return

# If neither Bash nor Zsh, `[p -o q ]` behaviour isn't well defined; don't care about other shells.
[ -n "$BASH_VERSION" ] || [ -n "$ZSH_VERSION" ] || return 0

[[ -n $BASH_VERSION ]] && shopt -s nullglob

[[ -n $ZSH_VERSION ]] && setopt NULL_GLOB
# ----

# Generic environment variable prepend & append operations.
# ----
# NOTE: Some `eval` magic for dynamic export updates.

# The first variable is the name of an environment variable & the second variable a value to append.
append_export() {
	local value="$2"
	local var="$1"

	[[ -z $var || -z $value ]] && return

	# Debug
	# eval "echo ${var}"

	case ":$(eval "echo \$$var"):" in
	*:$value:*) ;;
	*)
		# NOTE: The `export` command can be left out.
		eval "export $var=\"\${$var:+\$$var:}\$value\""
		;;
	esac
}

# The first variable is the name of an environment variable & the second variable a value to
# prepend.
prepend_export() {
	local value="$2"
	local var="$1"

	[[ -z $var || -z $value ]] && return

	case ":$(eval "echo \$$var"):" in
	*:$value:*) ;;
	*)
		eval "export $var=\"\$value\${$var:+:\$$var}\""
		;;
	esac
}

# Append (& prepend) the default paths.
appendpath() {
	append_export "PATH" "$1"
}

prependpath() {
	prepend_export "PATH" "$1"
}

appendmanpath() {
	append_export "MANPATH" "$1"
}

appendinfopath() {
	append_export "INFOPATH" "$1"
}

appendlibrarypath() {
	append_export "LD_LIBRARY_PATH" "$1"
}

prependlibrarypath() {
	prepend_export "LD_LIBRARY_PATH" "$1"
}

# FIX: Sort out `perlbin.sh`'s dependency.
append_path() {
	appendpath "$1"
}

appendpath '/usr/local/sbin'
appendpath '/usr/local/bin'
appendpath '/usr/bin'

appendmanpath '/usr/share/man:/usr/local/share/man'
appendinfopath '/usr/share/info:/usr/local/share/info'
# ----

# NOTE: This section will break stuff if not customized.
# ----
# Define alternative paths (shared across users) for $HOME directory content, accessible via
# environment exports. Only the first available directory is used.
alt_homes=("/mnt/External")
# NOTE: Default the `ALT_HOME` export to current user's `HOME`
[[ -z "$ALT_HOME" ]] && export ALT_HOME="$HOME"

# Preference to use a terminal-based browser, set any value.
export PREFER_TEXT_BROWSER=

# Wine stuff
export DXVK_HUD="fps"

# Libva video acceleration
# Intel
export LIBVA_DRIVER_NAME="iHD"
# Nvidia (nouveau)
# export LIBVA_DRIVER_NAME="nvidia"

# VDPAU video acceleration
# export VDPAU_DRIVER="va_gl"
# Nvidia (proprietary)
export VDPAU_DRIVER="nvidia"

# NVIDIA optimus stuff
# (pbo|sync) pbo is faster than sync.
export VGL_READBACK="pbo"

# Mesa stuff
# Disable vsync for the primus bridge.
# export VBLANK_MODE=0

# Synchronize the primus display thread with the application's rendering thread, for compositing
# window managers.
# export PRIMUS_SYNC=1
# ----

# `export`s & `unset`s
# ----
# Tmux stuff.

# Enable nested tmux sessions {Not a good idea}.
# [[ -z $TMUX ]] && unset TMUX
# ----

# Library preload
# ----
# Use jemalloc's memory allocator implementation.
# NOTE: Steam doesn't play nice with this, requiring the guard.
# [[ $(whoami) != *"wine"* ]] &&
# prependlibrarypath "/usr/lib/libjemalloc.so"

# Set defaults
# ----
# export XDG_RUNTIME_DIR="$HOME"
export COUNTRY="KE"
export EDITOR="vim"
export IGNOREEOF=50
export MAIL="${MAIL:-/var/mail/$USER}"
# Some programs don't play nice without `raw`ing ANSI colour sequences.
export PAGER="${PAGER:-less -P?n -R}"
export SUDO_EDITOR="vim"
export VISUAL="vim"

# REF: grml-zsh
# Support colors in less.
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

# Termcap is outdated, old, and crusty, kill it.
unset TERMCAP

# Directory exports
# Export XDG Paths (if not set).
if [[ -d $HOME && -z $XDG_CONFIG_HOME ]]; then
	export XDG_CACHE_HOME="$HOME/.cache"
	export XDG_CONFIG_HOME="$HOME/.config"
	export XDG_DATA_HOME="$HOME/.local/share"
fi
# ----

# nnn stuff
if [[ -x "$(command -v nnn)" ]]; then
	[[ -x "$(command -v pistol)" ]] && export USE_PISTOL=1

	export GUI=1 export NNN_BMS="D:~/Downloads/;G:~/git-repos;e:/etc;d:/dev;m:/media;M:/mnt;o:/opt;t:/tmp;u:/usr;v:/var" export NNN_FIFO="/tmp/nnn.fifo"
	export NNN_OPTS="daExc"
	export NNN_USE_EDITOR=1

	export NNN_PLUG_MEDIA='m:-mediainf;v:-preview-tui'
	export NNN_PLUG_MODE='w:!chmod +w $nnn*;W:!chmod -w $nnn*'
	export NNN_PLUG='g:-!git log --all --graph*;p:-!less -iR $nnn*;d:diffs;'"$NNN_PLUG_MODE;$NNN_PLUG_MEDIA"

	# REF: https://github.com/jarun/nnn/wiki/Usage#program-options.
	[[ -x "$(command -v atool)" || -x "$(command -v bsdtar)" ]] &&
		export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip|zst)$"
fi

# Disable libmtp auto-probing.
export MTP_NO_PROBE=1

# Disable GNOME accessibility bridge warnings.
export NO_AT_BRIDGE=1

# Use qt5ct/qt6ct to set the Qt theme.
export QT_QPA_PLATFORMTHEME="qt5ct"
# Workaround for the breakage above.
#export XDG_CURRENT_DESKTOP=GNOME

drive_counter=0
for drive in "${alt_homes[@]}"; do
	[[ ! -d "$drive" ]] && continue

	if [[ "$drive_counter" -eq 0 ]]; then
		ALT_HOME="$drive"
	else
		eval "export ALT_HOME$drive_counter=\"$drive\""
	fi

	drive_counter=$((drive_counter + 1))
done

# TODO: Customize with own paths.
# Single entry per export; keeps handling logic sane.
# Paths containing ":" will break.
paths_list=(
	"ANDROOT:$ALT_HOME/Android"
	"ASPROOT:$ALT_HOME/Downloads/ASP"
	"DEV_SYNC_DIR:$ALT_HOME/Sync"
	"DOWNLOADS:$ALT_HOME/Downloads"
	"DROPBOX_DIR:$ALT_HOME/Dropbox"
	"MUSIC:$ALT_HOME/Music"
	"PICTURES:$ALT_HOME/Pictures"
	"VIDEOS:$ALT_HOME/Videos"
	"WALLPAPERS:$ALT_HOME/Pictures/Wallpaper"
	"WALLPAPERS:/usr/share/backgrounds/archlinux"
	"ANT_HOME:/usr/share/apache-ant"
)

# (text-mode) browser list, sorted by preference
browser_list=("w3m" "lynx" "elinks" "links")
if [[ -z $PREFER_TEXT_BROWSER ]]; then
	# GUI browsers prepended to array, sorted by preference
	browser_list=("waterfox-classic" "waterfox-current" "firefox" "surf" "chromium"
		"epiphany" "opera" "${browser_list[@]}")
fi

# Debug
# echo "${#browser_list}" "${browser_list[*]}"

for _browser in "${browser_list[@]}"; do
	[[ -x $(command -v "$_browser") ]] &&
		BROWSER="$(command -v "$_browser")" && export BROWSER

	# Debug
	# echo "$surf" "$BROWSER"

	[[ -n $BROWSER ]] && break
done
# ----

# TODO: Look into resetting path exports when the profile is sourced; otherwise append; reset should
# occur at "/etc/profile".
for item in "${paths_list[@]}"; do
	export_name="$(cut -d : -f 1 <<<"$item")"
	export_path="$(cut -d : -f 2 <<<"$item")"

	[[ ! -d $export_path ]] && continue

	# Debug
	# echo "export \"$export_name\"=\"$export_path\""

	append_export "$export_name" "$export_path"
done

[[ -z $KERNEL_LIBS ]] &&
	KERNEL_LIBS="/lib/modules/$(uname -r)/build/include" && export KERNEL_LIBS

# Set boost entry.
# [[ -z $BOOST_ROOT && -d /usr/include/boost ]] &&
# export BOOST_ROOT="/usr/include/boost"

# NVIDIA Vulkan support.
[[ -z $VK_ICD_FILENAMES && -r "/usr/share/vulkan/icd.d/nvidia_icd.json" ]] &&
	export VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/nvidia_icd.json"

# Gradle entry.
[[ -d "$HOME/.gradle" ]] &&
	export GRADLE_HOME="$HOME/.gradle" &&
	export GRADLE_OPTS=-Xmx4G

# Maven
if [[ -x $(command -v mvn) ]]; then
	export MAVEN_OPTS=-Xmx4G
	# export M2_HOME=/opt/maven
	# appendpath "$M2_HOME/bin"
fi

# Code stuff
CC="$(command -v gcc)" && export CC
CXX="$(command -v g++)" && export CXX
PYTHON="$(command -v python)" && export PYTHON

# Attempt at setting tar compression flags.
# shellcheck disable=SC2089,SC2090
[[ -z $TAR_C_FLAGS ]] &&
	TAR_C_FLAGS="-I 'xz -9e -T $(nproc)'" && export TAR_C_FLAGS
# ----

# $PATH exports
# ----
# NOTE: use `appendpath` & `prependpath` as you deem fit.

# Set the list of directories that cd searches.
# CDPATH=(
#    $CDPATH
# )

# Java
if [[ -d /usr/lib/jvm/ ]]; then
	if [[ -z $JAVA_HOME ]]; then
		java_default="/usr/lib/jvm/default"

		# NOTE: Use the default jvm or the lowest installed version as default.
		if [[ -d "$java_default" ]]; then
			export JAVA_HOME="$java_default"
		else
			# Remove "/usr/lib/jvm/default/bin" entry from the `PATH` export.
			PATH=${PATH//$java_default/}

			for java_path in /usr/lib/jvm/*; do
				[[ ! -d "$java_path" ]] && continue

				export JAVA_HOME="$java_path"
				appendpath "$JAVA_HOME/bin"

				break
			done
		fi
	fi

	# -Xmx4G
	#  for ZGC on Java 11-14
	export JDK_JAVA_OPTIONS_EXTRA="-XX:+HeapDumpOnOutOfMemoryError -XX:+UnlockExperimentalVMOptions -XX:+UseZGC"
	# [[ -z "$JDK_JAVA_OPTIONS" ]] &&
	export JDK_JAVA_OPTIONS="-Dfile.encoding=UTF-8 -Dawt.useSystemAAFontSettings=on \
		-Dswing.aatext=true -Dsun.java2d.opengl=true \
		-Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel \
		-Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
fi

# TexLive
if [[ -d /opt/texlive/bin/x86_64-linux ]]; then
	# Main TeX directory
	export TEXDIR="/opt/texlive"
	# Directory for site-wide local files
	export TEXMFLOCAL="$TEXDIR/texmf-local"
	# Directory for variable and automatically generated data
	export TEXMFSYSVAR="$TEXDIR/texmf-var"
	# Directory for local config
	export TEXMFSYSCONFIG="$TEXDIR/texmf-config"

	appendpath "$TEXDIR/bin/x86_64-linux"
	appendmanpath "$TEXDIR/texmf-dist/doc/man"
	appendinfopath "$TEXDIR/texmf-dist/doc/info"
else
	export TEXMFDIST=/usr/share/texmf-dist
	export TEXMFLOCAL=/usr/local/share/texmf:/usr/share/texmf
	export TEXMFSYSVAR=/var/lib/texmf
	export TEXMFSYSCONFIG=/etc/texmf
fi

# The below exports include references to `$HOME`.
# Directory for user-specific files
export TEXMFHOME="$HOME/.texlive/texmf"
# Personal directory for variable and automatically generated data
export TEXMFVAR="$HOME/.texlive/texmf-var"
# Personal directory for local config
export TEXMFCONFIG="$HOME/.texlive/texmf-config"
export TEXMFCACHE="$TEXMFSYSVAR;$TEXMFVAR"

# Perl5
if [[ -x $(command -v cpan) ]]; then
	# Remove "/usr/bin/site_perl" entry from the `PATH` export
	site_perl="/usr/bin/site_perl"
	PATH=${PATH//$site_perl/}
fi

# References cpan config's output.
if [[ -d $HOME/perl5 ]]; then
	export PERL_HOME="$HOME/perl5"
	appendpath "$PERL_HOME/bin"

	export PERL5LIB="$PERL_HOME/lib/perl5"
	export PERL_LOCAL_LIB_ROOT="$PERL_HOME"
	export PERL_MB_OPT="--install_base \"$PERL_HOME\""
	export PERL_MM_OPT="INSTALL_BASE=$PERL_HOME"

	# If you have modules in non-standard directories you can add them here.
	# export PERLLIB=dir1:dir2
fi

# Go
if [[ -z $GOPATH && -x $(command -v go) ]]; then
	export GOPATH="$HOME/go"
	appendpath "${GOPATH}/bin"

	# Set optimization level for Go binaries.
	export GOAMD64=v3
	# Disable proxy.golang.org usage.
	export GOPROXY=direct

	if [[ -d /usr/lib/go ]]; then
		export GOROOT="/usr/lib/go"
		appendpath "$GOROOT/bin"
	fi
fi

# Lua entry
[[ -d "$HOME/.luarocks/bin" || -x $(command -v luarocks) ]] &&
	appendpath "$HOME/.luarocks/bin"

# Haskell
[[ -d "$HOME/.cabal" ]] &&
	appendpath "$HOME/.cabal/bin"

# Rust
CARGO_BIN_DIR="$HOME/.cargo/bin"
if [[ -d $CARGO_BIN_DIR ]]; then
	appendpath "$CARGO_BIN_DIR"

	# NOTE: This may cause weird compile issues, unset if that happens.
	# [[ -x "$CARGO_BIN_DIR/sccache" ]] && export RUSTC_WRAPPER=sccache
fi

[[ -x $(command -v jupyter) ]] &&
	export JUPYTERLAB_DIR="$HOME/.local/share/jupyter/lab"

# ~/.local/bin
[[ -d $HOME/.local/bin ]] &&
	appendpath "$HOME/.local/bin"

[[ -d $HOME/.npm-global/bin ]] &&
	prependpath "$HOME/.npm-global/bin"

# Ruby
if [[ -x $(command -v ruby) ]]; then
	# export GEM_HOME=$(ruby -e "print Gem.user_dir")
	export GEM_HOME=$(gem env user_gemhome)

	appendpath "$GEM_HOME/bin"

	if [[ $GEM_HOME == "*$HOME*" && -d "$HOME/.rvm" ]]; then
		# shellcheck source=/dev/null

		# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
		[[ -d "$HOME/.rvm/bin" ]] && appendpath "$HOME/.rvm/bin"

		# Load RVM into shell session as a function
		[[ -r "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
	fi
fi
# ----

# Compiler flags
# ----
# Using CMake to handle project libraries and linker stuff.
# export LINKERFLAGS=( "-I/usr/include/tirpc" "-lnsl" "-ltirpc" "-lsctp" )

# NOTE: Don't use `-pipe` on systems with <=4GB to compile large packages.
# To check the amount of available memory execute:
# `grep -i "MemTotal" /proc/meminfo | sed -E 's/.*\s+([0-9]+).*/\1/'`

# NOTE: The usage of `-pipe` requires an assembler that can read from pipes in addition to temporary
# files, e.g.: the GNU assembler.

# Array of useful C/C++ warning flags.
#
# REF: man(1) gcc:
#
# `-Wpedantic` Issue all the warnings demanded by strict ISO C and ISO C++; reject all programs that
# use forbidden extensions, and some other programs that do not follow ISO C and ISO C++.
# For ISO C, follows the version of the ISO C standard specified by any -std option used.
warnings=("-Wall" "-Wextra" "-Wpedantic" "-Wfloat-equal" "-Wvla")

linker_flag="-fuse-ld="
if [[ -x "$(command -v mold)" ]]; then
	linker_flag+="mold"
elif [[ -x "$(command -v lld)" ]]; then
	linker_flag+="lld"
else
	linker_flag=""
fi

# Array of common C/C++ compilation flags.
#
# NOTE: Possible linker flags: `-fuse-ld=gold`, `-fuse-ld=lld`.
# NOTE: Specify `-flto` on a case-by-case basis to avoid LTO-ing libraries.
generic_flags=("-march=native" "-mtune=native" "-O2" "-pipe" "-fno-plt" "$linker_flag"
	"-fexceptions" "-Wp,-D_FORTIFY_SOURCE=3" "-Wformat" "-Werror=format-security"
	"-fstack-clash-protection" "-fcf-protection -fno-omit-frame-pointer
	-mno-omit-leaf-frame-pointer" "${warnings[*]}")

generic_flags_strict=("${generic_flags[*]}" "-fhardened" "-Werror" "-fsanitize=address"
	"-fsanitize=pointer-compare" "-fsanitize=pointer-subtract")

# Array of C/C++ debug-mode compile flags.
debug_flags=("-O0" "-ggdb" "-pipe" "${warnings[*]}" "-ftrapv" "-fopt-info"
	"-fvar-tracking-assignments")

# Final array of C/C++ compilation flags.
cflags=("${generic_flags[*]}" "-Wstrict-prototypes")
cxxflags=("${generic_flags[*]}" "-Wp,-D_GLIBCXX_ASSERTIONS")

# Use the default GCC C/C++ language standards.
cflags_strict=("${generic_flags_strict[*]}" "-Wstrict-prototypes")
cxxflags_strict=("${generic_flags_strict[*]}" "-Wp,-D_GLIBCXX_ASSERTIONS")

# Since one can't use arrays as environment variables; exporting the array contents as strings.
export CFLAGS=${cflags[*]}
export CFLAGS_STRICT=${cflags_strict[*]}

export CXXFLAGS=${cxxflags[*]}
export CXXFLAGS_STRICT=${cxxflags_strict[*]}

export LDFLAGS="-Wl,-O1 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now \
	-Wl,-z,pack-relative-relocs"

export DEBUG_FLAGS=${debug_flags[*]}

export MAKEFLAGS
MAKEFLAGS="-j$(nproc)"
# ----

# Proxy options
# ----
# Set to desired IP address
# export FTP_PROXY=''
# export HTTP_PROXY=''
# export HTTPS_PROXY=''
# export SOCKS_PROXY=''
# ----

# Import customized `PATH` into systemd environment.
[[ -x "$(command -v systemctl)" ]] &&
	dbus-update-activation-environment --systemd --all
# systemctl --user import-environment PATH HOME WALLPAPERS LD_PRELOAD LD_LIBRARY_PATH

# Configure ssh-agent
# ----
[[ "$(pgrep -cu "$USER" ssh-agent)" -lt 1 ]] &&
	ssh-agent -t 1h >"$XDG_RUNTIME_DIR/ssh-agent.env"
[[ ! -f "$SSH_AUTH_SOCK" ]] &&
	. "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
# ----

# Remove `blank` path entries.
PATH=${PATH//::/:}
INFOPATH=${INFOPATH//::/:}
MANPATH=${MANPATH//::/:}

export PATH
export MANPATH
export INFOPATH

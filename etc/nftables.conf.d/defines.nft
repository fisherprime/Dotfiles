# Interfaces.

# NOTE: Modify the definitions in this file.

# Interfaces
# ----
# Ethernet interfaces.
define nic_eth_inet = enp109s0f1

# Bonded wired & wireless interface.
define nic_bond = bond0

# Wi-Fi interfaces.
define nic_wifi_ap = wlan_ap
define nic_wifi_inet = wlan0
define nics_wifi = {
	$nic_wifi_ap,
	$nic_wifi_inet
}

# Remote network driver interface specification (RNDIS) interfaces; provides networking over USB.
# 
# Populate with created RNDIS interfaces on each/frequently used USB port(s) on your machine (one
# for each USB port).
define nic_rndis_inet0 = enp0s20f0u1
define nic_rndis_inet1 = enp0s20f0u5
define nic_rndis_inet2 = enp0s20f0u6
define nics_rndis = {
	$nic_rndis_inet0,
	$nic_rndis_inet1,
	$nic_rndis_inet2
}

# VM & lightweight container interfaces.
# ----
# Docker interfaces.
define nic_docker_inet = docker0
define nic_docker_bridge = br-*
define nics_docker = { nic_docker_inet }

# Libvirt interfaces.
define nic_libvirt0 = virbr0
define nics_libvirt = { $nic_libvirt0 }

# Qemu interfaces.
# define nic_qemu_<sth>
# ----

# Source network interfaces for packet forwarding.
define nics_forward_source = {
	$nic_wifi_ap,
	$nics_libvirt
}

# Trusted interfaces.
define nics_trusted = {
	$nic_bond,
	$nics_forward_source,
	$nics_rndis,
	$nics_wifi
}
# ----

# Network ranks.
# ----
# Local software access point networks.
define net_wifi_ap = 192.168.12.0/24

# External Wi-Fi networks.
define net_wifi_inet0 = 192.168.43.0/24
# define net_wifi_inet1 = 172.20.10.0/28
define nets_wifi_inet = { $net_wifi_inet0 }

# Local debugging networks.
define net_debug0 = 192.168.35.0/24
define nets_debug = { $net_debug0 }

# VM & lightweight container networks.
# ----
# Docker network.
define net_docker_inet0 = 172.17.0.0/24
define nets_docker = { $net_docker_inet0 }

# Libvirt VM networks.
define net_libvirt0 = 192.168.122.0/24
define nets_libvirt = { $net_libvirt0 }
# ----

# Private networks.
# 
# These network ranks will have their IPs masqueraded for NAT (add networks as necessary).
#
# $nets_docker, $nets_libvirt
define nets_private = {
	$net_wifi_ap,
	$nets_debug,
}

# Trusted networks.
define nets_trusted = {
	$nets_private,
	$nets_wifi_inet
}
# ----

# Some network nodes.
# ----
define gateway_debug0 = 192.168.35.1

# define gateway_docker_bridge1 = 172.18.0.1
define gateway_docker_inet0 = 172.17.0.1

define gateway_wifi_ap0 = 192.168.12.1
# ----

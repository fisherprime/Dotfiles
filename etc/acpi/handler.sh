#!/bin/bash
# Default ACPI script that takes an entry for all actions

log() { logger -p daemon.warning "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }

# Debug
# log "$@"

case "$1" in
button/power)
	case "$2" in
	PBTN | PWRF)
		log 'PowerButton pressed'
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
button/sleep)
	# Resolve system freeze if intel AX210's  bluetooth is on.
	bluetoothctl power off

	case "$2" in
	SLPB | SBTN)
		log 'SleepButton pressed'
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
button/lid)
	# Resolve system freeze if intel AX210's  bluetooth is on.
	bluetoothctl power off

	case "$3" in
	close)
		log 'LID closed'
		;;
	open)
		log 'LID opened'
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
cd/play)
	case "$2" in
	CDPLAY)
		# NOTE: Don't enable the below line unless MPD is run system wide (not
		# per user).
		# mpc toggle
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
video/switchmode)
	log "$@"
	case "$2" in
	VMOD)
		log "Video mode changed"
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
thermal_zone)
	log "$@"
	case "$2" in
	LNXTHERM:00)
		# Do something
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
processor)
	log "$@"

	# NOTE: Not sure what these ACPI values reference.
	case "$2" in
	LNXCPU:00)
		# Do something
		;;
	LNXCPU:01)
		# Do something
		;;
	LNXCPU:02)
		# Do something
		;;
	LNXCPU:03)
		# Do something
		;;
	LNXCPU:04)
		# Do something
		;;
	LNXCPU:05)
		# Do something
		;;
	LNXCPU:06)
		# Do something
		;;
	LNXCPU:07)
		# Do something
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
button/left | button/down | button/up | button/right)
	# Do nothing
	;;
video/brightness* | battery | ac_adapter* | button/volume* | button/mute)
	# Do nothing for events handled in separate files.
	;;
TOS*)
	# Do nothing for `Toshiba` events.
	;;
ABBC0F6B-8*)
	# Do nothing for `tuxedo_keyboard` events.
	# log '`tuxedo_keyboard` logged event:' "$@"
	;;
button/kbdillum*) ;;
*)
	uhd "$@"
	;;
esac

# vim:set ts=4 sw=4 ft=sh et:

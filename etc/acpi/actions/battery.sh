log() { logger -p daemon.warning "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }

case "$2" in
BAT0 | PNP0C0A:00)

	# Not sure what to do with these values.
	# case "$3" in
	# # 00000080)
	# *0)
	# log 'Battery state changed'
	# ;;
	# *1)
	# log 'Battery state unchanged'
	# ;;
	# *)
	# uhd "$@"
	# ;;
	# esac
	;;
*)
	uhd "$@"
	;;
esac

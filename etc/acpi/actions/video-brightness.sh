log() { logger -p daemon.warning "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }

# Using `tee` clogs up the journal log

adjust_percentage=5

if [[ -z $BACKLIGHT_CTRL_DIR ]]; then
	for dir in /sys/class/backlight/*; do
		[[ -d $dir ]] && export BACKLIGHT_CTRL_DIR="$dir"

		# Prefer ACPI controlled backlight
		[[ $dir == *"acpi_"* ]] && break
	done
fi

current_brightness=$(($(cat "$BACKLIGHT_CTRL_DIR/actual_brightness") + 0))
max_brightness=$(($(cat "$BACKLIGHT_CTRL_DIR/max_brightness") + 0))

case "$1" in
video/brightnessup)
	case "$2" in
	BRTUP)
		new_brightness=$((current_brightness + ((adjust_percentage * max_brightness) / 100)))
		[[ $new_brightness -gt $max_brightness ]] && new_brightness=$max_brightness

		printf "%s" "$new_brightness" >"$BACKLIGHT_CTRL_DIR/brightness"
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
video/brightnessdown)
	case "$2" in
	BRTDN)
		new_brightness=$((current_brightness - ((adjust_percentage * max_brightness) / 100)))
		[[ $new_brightness -lt 0 ]] && new_brightness=0

		printf "%s" "$new_brightness" >"$BACKLIGHT_CTRL_DIR/brightness"
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
esac

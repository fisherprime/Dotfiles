log() { logger -p daemon.warning "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }

adjust_percentage=5

# NOTE: Volume changes using decibel values is logarithmic rather than liner.
#
# This is the case when using amixer.

case "$1" in
button/mute)
	case "$2" in
	MUTE)
		# ALSA mixer.
		amixer set Master toggle
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
button/volumeup)
	case "$2" in
	VOLUP)
		# ALSA mixer.
		amixer set Master "$adjust_percentage"%+
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
button/volumedown)
	case "$2" in
	VOLDN)
		# ALSA mixer.
		amixer set Master "$adjust_percentage"%-
		;;
	*)
		uhd "$@"
		;;
	esac
	;;
esac

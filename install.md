# Installation instructions for Arch Linux

    Based on my system: i7-7700HQ, GTX 1060.
    File contents specified in this guide are neither complete nor up-to-date.

# Assumptions

    1. <install-device> is the block device you want to install to.
    1. <boot-partition> is the boot partition.
    1. <luks-container> is LUKS container.
    1. <decrypt-root> is the mapping you want to use for your opened device.
    1. <vol-group> is the name for the volume group you desire to create.

# Bootable installation media environment

---

---

**Root partition should be at least 50GB.**

## Configure environment

```sh
loadkeys us
timedatectl set-ntp true
timedatectl status
```

## Show disk info

```sh
fdisk -l # Display disk partition table.
lsblk -f # List block device information & show filesystem info.
```

## Partition drives (using parted)

```sh
parted <install-device>
```

**Inside the parted interactive CLI**
[Official partitioning guide](https://wiki.archlinux.org/index.php/Partitioning)

```sh
mklabel gpt
mkpart primary fat32 1 513 # Size in MB is implied
mkpart primary 514 100% # Container Doesn't have to be aligned
set 1 boot on
```

## Setup encrypted LVM on LUKS

[Official disk encryption guide](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system)

### Encrypt partitions

**Specified hash, cipher & key size vaues are examples.**

```sh
cryptsetup -vy --hash=sha512 --cipher=serpent-xts-plain64 --key-size=512 luksFormat <luks-partition> # Create LUKS container.
cryptsetup config <luks-partition> --lable <luks-partition-label> # Configure the LUKS partition label.
cryptsetup luksDump <luks-partition>
cryptsetup open <luks-partition> <decrypt-root> # Can also use `cryptsetup luksOpen <luks-partition> <decrypt-root>`.
```

### Create logical volumes

```sh
pvcreate /dev/mapper/<decrypt-root> # Create physical volume.
vgcreate <vol-group> /dev/mapper/<decrypt-root> # Create Volume group.
lvcreate -L <size> <vol-group> -n <logical-volume-name> # Create logical volumes for: root, swap, home, var, ... partitions.
lvcreate -l 100%FREE <vol-group> -n <logical-volume-name> # Create logical volume from remaining free space (using percentages).
```

## Format partitions (LVM)

**/dev/mapper/<vol-group>-<logical-volume-name> and /dev/<vol-group>/<logical-volume-name> point to the same thing.**
**`mkfs -O encrypt` enables fscrypt for directory-level encryption**
**For BIOS systems one can use `ext4`, for EFI systems one is limited to `FAT32`**

```sh
mkfs.fat -F32 <boot-partition>
mkfs -t <partition-type> /dev/mapper/<vol-group>-<logical-volume-name> # Format the logical volumes.
mkswap /dev/mapper/<vol-group>-<swap-volume-name> # Setup the swap area if LVM created.
```

## Mount partitions in preparation for package installation

```sh
mount /dev/mapper/<vol-group>-<root-volume-name> /mnt

mkdir /mnt/boot
mkdir /mnt/home
mkdir /mnt/etc

mount <boot-partition> /mnt/boot # Mount the boot partition.
mount /dev/mapper/<vol-group>-<home-volume-name> /mnt/home # If the LVM was created.
swapon /dev/mapper/<vol-group>-<swap-volume-name> # If the LVM was created.

e2label /dev/mapper/<vol-group>-* <label> # Change the label on ext(2|3|4) filesystems.
swaplabel -L <label> /dev/mapper/<vol-group>-<swap-volume-name> # Label swap partition.

lsblk -f # List block devices showing: label, UUID and mount point.
genfstab -U /mnt >> /mnt/etc/fstab # Populate fstab for the partitioned system.
```

## Install packages to the new system

**`linux` can be replaced with an alternative kernel: linux-zen, linux-ck, …**

```sh
pacstrap /mnt base base-devel linux linux-firmware vim dhcpcd
```

**For systems using grub, the grub package is necessary.**

```sh
pacstrap /mnt grub
```

## Append the cryptdevice UUID to /etc/default/grub (grub specific config)

**This operation copies the UUID for the created luks container to the grub config; allows for a
cut-and-paste into the "GRUB_CMDLINE_LINUX" directive.**

```sh
lsblk -no UUID <luks-partition> >> /etc/default/grub
```

## Append swap partition fstab entry

**This operation is necessary only if the swap partition wasn't mounted when `genfstab` updated
"/mnt/etc/fstab".**

```sh
printf "UUID=%s		none      	swap      	defaults  	0 0\n" "$(lsblk -no UUID /dev/mapper/<vol-group>-<swap-volume-name>)"
```

## Append CD/DVD drive fstab entry (if a CD/DVD drive exists)

```sh
printf "/dev/cdrom 		/mnt/DVD       none	      	rw 0 0\n" >> /mnt/etc/fstab
```

## Network config enable interface and request DHCP IP

```sh
ip link
ip link set <interface> up
dhcpcd <interface>
```

## Switch to /mnt as the new root

```sh
arch-chroot /mnt
```

# chroot environment

---

---

## Define timezone

---

```sh
ln -sf /usr/share/zoneinfo/<Region>/<City> /etc/localtime
hwclock --systohc
```

## Setup locale

---

**If unsure of the appropriate locale, default to `en_US.UTF-8` for English**

Uncomment appropriate values in /etc/locale.gen, e.g.:

    en_US.UTF-8 UTF-8
    en_GB.UTF-8 UTF-8

Then generate locales:

```sh
locale-gen
```

Set the keymap for the virtual console in "/etc/vconsole.conf":

```conf
KEYMAP=unicode
```

Set the desired system locales in "/etc/locale.conf", e.g.:

```conf
# Default locate
LANG=en_GB.utf8 # Or en_US.utf8

# Default locale with a list of fallbacks
LANGUAGE=en_GB.utf8:en_US.utf8

# Sort dotfiles first
LC_COLLATE=C

# Date-time format
# LC_TIME=
```

## Setup hostsname

---

Set hostname in "/etc/hostname", e.g.:

```conf
my-pc
```

## Install packages

---

**Install the necessary hardware video acceleration package for your system**:

    libva-mesa-driver # For VA_API GPU video acceleration (open source).
    mesa-vdapu # For VDPAU GPU video acceleration (open source).

    nvidia-utils # Proprietary nvidia video acceleration.

```sh
pacman -S - < packages/pacman.txt # Install all but AUR packages.
<aur-wrapper> -S - < packages/packages.txt # Install all (including AUR packages).
```

Prefer using virtual environments or install Python pip packages with the
`--user` flag.
This will avert conflicts that arise from system & pip package version
conflics.

## Configure the initial ramdisk (dracut)

---

REF: [ArchWiki Dracut](https://wiki.archlinux.org/index.php/Dracut)

Ensure the following kernel parameters are defined, within dracut's cmdline (in
"/etc/dracut.conf.d/\*.conf") or the bootloader's config:

```
rd.luks.name=$(cryptsetup luksUUID /dev/disk/by-label/cryptroot)=<decrypt-root>
root=UUID=$(blkid -o value -s UUID /dev/disk/by-label/<root-label>)
resume=UUID=$(blkid -o value -s UUID /dev/disk/by-label/<swap-label>)
```

### Create initial ramdisk environments, for all kernels

**The `--no-hostonly-cmdline` option should be omitted if it is desired to store the needed kernel
parameters within the initramfs.**

**The `--kver` option should be specified if multiple kernels are installed to build the desired
initramfs for each kernel.**

Create the stripped down --only necessary modules-- initramfs (as root):

```sh
dracut --force --no-hostonly-cmdline
/boot/<initramfs-name>.img --kver '<kernel-version>'
```

Create the fallback initramfs (as root):

```sh
dracut --force --no-hostonly-cmdline -N
/boot/<initramfs-name>-fallback.img --kver '<kernel-version>'
```

**The above operations generate kernel images using the running kernel
(installation media's kernel) not the pacstraped one.**

#### Fix lvmetad failures for `mkinitcpio` (if enountered)

This will occur when a user has partitioned their system to use LVM. Should the "/run" folder lack
from the `arch-chroot` environment, `lvmetad` operations will fail.
The solution is to bind the installation media(host)'s "/run" folder to the chroot system's "/run"
folder.

##### In a new tty (Ctrl + Alt + F(2|3|4...7))

```sh
mkdir -p /mnt/hostrun/lvm && mount --bind /run /mnt/hostrun && \
    mount --bind /mnt/hostrun/lvm /mnt/run/lvm
```

#### Back in the tty with the `lvmetad` failure

Rebuild the initramfs:

```sh
dracut --force --no-hostonly-cmdline \
/boot/<initramfs-name>.img --kver '<kernel-version>'
```

```sh
dracut --force --no-hostonly-cmdline -N \
/boot/<initramfs-name>-fallback.img --kver '<kernel-version>'
```

## Configure grub (if grub is the chosen bootloader) "/etc/default/grub"

---

```conf
# Can use LABEL, UUID or path
GRUB_CMDLINE_LINUX="cryptdevice=UUID=<UUID>:<vol-group> root=/dev/mapper/<vol-group>-<root-volume-name> resume=/dev/mapper/<vol-group>-<swap-volume-name> crypto=<hash-algorithm>:<cipher>:<key-size>:0:"
# If no encryption was set up, use: crypto=::::
```

### Install EFI grub

Install `efibootmgr` (for UEFI compatible systems) and `os-prober` (if additional operating systems
are installed on the system):

```sh
pacman -S efibootmgr os0prober
```

**To set up a custom EFI entry name, append the option `--bootloader-id=<cool-name>` to
`grub-install`.**

```sh
grub-install --target=x86_64-efi --efi-directory=/boot
```

For BIOS only systems, refer to:
https://wiki.archlinux.org/title/GRUB#GUID_Partition_Table_(GPT)_specific_instructions
and
https://www.gnu.org/software/grub/manual/grub/html_node/BIOS-installation.html#BIOS-installation

1. Create a 2MB partition for GRUB's core.img; enable the `bios_grub` flag on the partition.
2. Create a 512MB partition to store the initramfs, & related stuff.
3. Install grub using: `grub-install --target=i386-pc /dev/<disk>` where `<disk>` is the device
   (not partition) where GRUB is to be installed.

Execute the command below to generate the grub config:

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```

## Exit & restart into installed environment

---

```sh
exit
sync
umount -l /mnt
shutdown -r now
```

# Installed environment

---

---

## Misc

---

```sh
timedatectl set-timezone <Region>/<City>
lspci | grep -e VGA -e 3D # Get VGA Controller model.
loginctl show-session $XDG_SESSION_ID # Show X session status (if an X11-based window manager is used).
useradd -m -G <additional> -s /bin/zsh {or other shell} <Uname>
```

## Network

---

**The `--now` flag will invoke `start` for a service as it's enabled.**
**The `--now` flag will also invoke `stop` for a service as it's disabled.**

```sh
systemctl enable --now dhcpcd.service
sudo ip link set <interface> up
sudo dhcpcd <interface> # Request IP, or -N for all active.
```

## Launch X on login (for X11-based window managers)

---

In the appropriate shell's user profile: bash => "~/.profile" or "~/.bash_profile", zsh => "~/.zprofile":

```sh
# Replace -eq 1 with -le <number of desired graphical sessions>
[ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ] &&
	exec startx
```

## Let non-root users execute some superuser commands (using sudo)

---

```conf
<user> <hostname> =NOPASSWD: /usr/bin/systemctl poweroff,/usr/bin/systemctl halt,/usr/bin/systemctl reboot
<user> <hostname> ALL= ALL # Need password for root; hostname not necessary.
<group> ALL = <terminals> # ALL ...
export VISUAL=vim; visudo
```

## Apply necessary modifications to user groups

---

**The docker group is a root group
don't use on untrusted users or forget its existence**

```sh
gpasswd -a <user> <group>
```

Groups:

    bumblebee **NVIDIA dGPU power toggle**
    cups **Printer management**
    docker **Docker management**
    kvm **Use VMs in KVM mode**
    libvirt **Enable libvirt management, not necessary when using kvm group**
    lp **Bluetooth device control**
    plugdev **Allow usage of ADB; needs to be created first**
    postgres **PostgreSQL management**
    uucp **Serial communication**
    video **Configured to allow manipulation of	/sys/class/brightness/*/brightness**
    wheel **The De facto sudo user group**
    wireshark

## Update font cache

---

```sh
fc-cache -fv
fc-cache-32 -fv # If using 32-bit packages(multilib repo).
```

## AUR packages

---

**Compile and install, or use an AUR wrapper**

```sh
git clone https://aur.archlinux.org/<package-name>.git
(vim|nano|gedit|cat...) PKGBUILD # Verify packging sanity.
makepkg -si
```

```sh
<aur-wrapper> -S <package-name>
```

```sh
<aur-wrapper> -S - < packages.txt
```

## pip3 packages

---

**Prefer `pipx` for installing Python applications, it installs each app to a separate a virtualenv.**

### install from list

```sh
pip install -r packages/pip3.txt
```

## Perl packages

---

One can use `cpan` (bundled with the `perl` package) or use the preferred `cpanm` executable (provided by the `cpanimus` package).
The latter is preferred in this case.

cpanm -i

    Bundle::CPAN

    AnyEvent::I3
    App::ucpan
    CPAN::DistnameInfo
    CPAN::Reporter
    Log::Log4perl
    Tk
    inc::latest

## ASP packages

---

**DKMS modules will not play nice if compiled with different CC/CXX versions to the kernel. Solution
is to build packages from the Arch Build System.**

## Golang packages

---

Most of the required tools are installed on calling `GoInstallBinaries` from
[vim-go](https://github.com/fatih/vim-go):

    github.com/golangci/golangci-lint/cmd/golangci-lint

Source with "go get":

## Rust components & packages

---

components:

    1. clippy
    1. rust-src
    1. rustfmt

Packages:

    1. bat
    1. cargo-bloat
    1. cargo-geiger
    1. cargo-lichking
    1. cargo-update
    1. clog-cli
    1. sccache

---

### Ensure a toolchain is setup

```sh
# Show toolchain help
rustup help toolchain
# Install a toolchain: stable, beta, nightly, ...
rustup toolchain install <toolchain>
# Specify a default toolchain
rustup default <toolchain>
```

### Add source code, formatter, language server components, clippy linter

```sh
rustup toolchain install nightly
rustup default nightly
rustup component add rust-src rustfmt clippy
```

### Missing packages

Review [rust-toolstate](https://rust-lang-nursery.github.io/rust-toolstate/) & the subsequent
[rustup-components-history](https://rust-lang.github.io/rustup-components-history/) pages to
identify the last successful build for a binary.

This may require rollbacks of a toolchain to get the desired binary.

### Rollback

**Date is in YYYY-MM-DD format; after this operation, components should be readded.**

```sh
rustup install <branch>-<date>
```

e.g:

```sh
rustup install nightly-2019-07-19
```

# Post install

---

---

## Resize LUKS partition and LVM

---

```sh
cryptsetup open <luks-partition> <decrypt-root>
# vgchange -a n <Volume Group> # Deactivete (degrade) Volume group.
cryptsetup close <decrypt-root> # Can also use cryptsetup luksClose <luks-partition> <decrypt-root>.
parted <install-device>
```

Inside the interactive parted instance:

```sh
resizepart 2 100% # Resize extended volume.
resizepart 5 100% # Resize logical volume.
quit
```

```sh
cryptsetup open <luks-partition> <decrypt-root>
cryptsetup resize <decrypt-root>

# vgchange -a y <Logical Volume Manager> # Activate Volume group (partial), `-a ay` (complete).

# Iterate over the adjusted volumes with this command.
e2fsck -f /dev/mapper/<vol-group>-<logical-volume-name>
resize2fs /dev/mapper/<vol-group>-<logical-volume-name>
```

To omit a package from upgrade edit the line with `IgnorePkg` to "/etc/pacman.conf":

```conf
IgnorePkg = <package-name>
```

## Systemd services & sockets

---

**The list of services can be concatenated (with a space in between) into one command**

For systemwide systemd units, run the command: `systemctl enable --now` for the following services
(if installed):

    acpid.service
    auditd.service
    bluetooth.service
    bumblebeed.service
    chronyd.service
    clamav-daemon.service
    clamav-freshclam.service
    dhcpcd.service
    dictd.service
    dnscrypt-proxy.service
    fangfrisch.timer
    fstrim.timer
    intel-undervolt.service
    iwd.service
    nftables.service
    pcscd.socket
    plocate-updatedb.timer
    spamassassin.service
    usbguard.service
    uwsgi@searx.service

**`pcscd.socket` is needed by yubikey-manager for smartcard stuff.**

For `arpwatch@.service` (systemd template unit), get the names for the
networking interfaces on the current machine & append the interface name
between "@" & ".": `systemctl enable --now arpwatch@wlan0.service`.

For user session systemd units, run the command:
`systemctl enable --global --now` (enable for all user login sessions) or
`systemctl enable --user --now` (enable for current user's login sessions) for
the following services (if installed):

    mpd.socket
    pipewire-pulse.service
    pipewire.service
    syncthing-resume.service
    syncthing.service
    wireplumber.service

**When using pipewire, PulseAudio should always run on the main user; this will allow the wine user
to play audio through the system's hardware.**

### Disable unnecessary services/sockets

This step should be performed if one doesn't use the listed services/sockets: `systemctl disable --now`:

    NetworkManager.service
    iptables.service
    systemd-journald-audit.socket

One can mask the unwanted services to make them impossible to start: `systemctl mask <service>`.

## Disable GNOME automount

---

```sh
gsettings set org.gnome.desktop.media-handling automount false
gsettings set org.gnome.desktop.media-handling automount-open false
```

## Set GRUB password

---

```sh
grub-mkpasswd-pbkdf2
```

### In /etc/grub.d/4\*

"40_custom"|"41_custom", any name really.

```conf
set superusers="username"
password_pbkdf2 username <password>
```

## File permissions recovery and restore

---

### Satisfactory way to save ACL

```sh
getfacl -R /boot /doc /usr > sys-backup-$(date +%Y-%m-%d\(%H:%M\)).acl
getfacl -R $(ls /mnt/D | grep -v -e Arch | sed -E "s/^/\/mnt\/D\//") /mnt/D > D-backup-$(date +%Y-%m-%d\(%H:%M\)).acl
```

### Restore ACL

```sh
setfacl --restore sys-backup.acl
```

## Dictionary

---

### StarDict

Package name: sdvc

```sh
mkdir -p /usr/share/stardict
mkdir -p /usr/local/share/stardict/dic
ln -s /usr/local/share/stardict/dic /usr/share/stardict/dic
```

1. get archives from:

   - [Abloz](https://web.archive.org/web/20140917131745/http://abloz.com/huzheng/stardict-dic/dict.org/) **Has the Collaborative International Dictionary of English**
   - [Jargon file](http://catb.org/jargon/download.html)
   - [Huzeng](http://download.huzheng.org/)

2. extract archives

**GoldenDict replaced stardict after it's copyright infringement issues.**

## PostgreSQL setup

---

```sh
sudo su - postgres -c "initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'"
```

## NodeJS setup

---

### User-local NodeJS global directory

Create the global directory with `mkdir ~/.local`; preferring "~/.local" for the binaries to be
included in the `$PATH` at ""~/.local/bin".

For npm execute: `npm config set prefix '~/.local'` while for yarn execute `yarn config set prefix '~/.local'`.

**Yarn will obtain the "prefix" value from "~/.npmrc" if it exists.**

## ClamAV setup

---

### Create socket

```sh
touch /var/lib/clamav/clamd.sock
chown clamav:clamav /var/lib/clamav/clamd.sock
```

## Matrix setup

---

```sh
cd /var/lib/synapse
sudo -u synapse python2 -m synapse.app.homeserver \
  --server-name my.domain.name \
  --config-path /etc/synapse/homeserver.yaml \
  --generate-config \
  --report-stats=yes
```

## Grub themes

---

For systems with a separate boot & root partition, copy over your desired grub theme from
"/usr/share/grub/themes" / a cloned repo into "/boot/grub/themes".

## Greetd setup
---

Install `greetd` & `greetd-tuigreet`, swap `agetty` for `tuigreet` in "/etc/greetd/config.toml" then
enable the service.

## Miscellaneous Fixes

---

### Sort out "sys-subsystem-net-devices-\*" service failure

```sh
rm /etc/systemd/system/multi-user.target.want/netctl@*.service
```

## Execute as other X11 user session shell script

---

```sh
#!/bin/bash

xhost +SI:localuser:otheruser
sudo -u otheruser env HOME=/home/otheruser USER=otheruser USERNAME=otheruser LOGNAME=otheruser /usr/bin/<program-name> "$@"
```

## Samba setup

---

### Setup a new samba user

```sh
sudo useradd samba -s /usr/bin/nologin
smbpasswd -a samba
sudo usermod --lock samba
```

### Alternatively

    Use an existing user

```sh
smbpasswd -a <user>
```

## Security

---

`umask`, sets the file mode creation mask which determines the initial value
of file permission bits for newly created files.
u=rwx,g=rx,o=rx (Default mask) 022
u=rwx,g=rx,o= (Preferred mask) 027

**Fucks up execution though, you'll have to be root to exeute**
Use this umask only for "/etc/profile.d":

    ```conf
    umask 027
    ```

### To show umasks in symbolic notation

```sh
umask -S
```

## Unbound setup

---

### Generate certificates for unbound remote control

```sh
unbound-control-setup
```

## Nvidia options

---

### In /etc/modprobe.d/nvidia.conf

1. Screen tearing:

   - Add `options nvidia_drm modeset=1`

2. Improved memory allocation:

   - **REF: [ArchWiki: NVIDIA/Tips_and_tricks](https://wiki.archlinux.org/index.php/NVIDIA/Tips_and_tricks)**
   - Check for PAT support with `grep pat /proc/cpuinfo`
   - Add `options nvidia NVreg_UsePageAttributeTable=1`

### Sort out audio issues

**This sets up PulseAudio for multiuser scenarios. These options can be set in a user's PulseAudio
configs located at ~/.config/pulse/**

Append to "/etc/pulse/client.conf"

```conf
default-server = unix:/tmp/pulse-socket
```

Append to "/etc/pulse/default.pa"

```conf
load-module module-native-protocol-unix auth-group=audio socket=/tmp/pulse-socket
```

## Notes

---

1. Configs specified in /etc is systemwide;

2. Project compilation speedup:

   1. Using ccache (will speed up future compiles of the same translation
      units)
   1. `export MAKEFLAGS="-j$(nproc)"`

3. Python packages not in the arch repos/outdated should have the pip version installed to the python user installation directory; and

4. Symlink the ~/.local folder for frequently used users to a directory in a large readable partition, Python tends to fill this space up.
